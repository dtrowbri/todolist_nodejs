const { List } = require('./lib/app/models/List');
const {Task} = require("./lib/app/models/Task");
const {User} = require("./lib/app/models/User");
const {ListDAO} = require("./lib/app/database/ListDAO");
const bodyParser = require('body-parser');

const express = require('express');
const app = express();
const port = 3000;

const dbHost = "localhost";
const dbPort = 3306;
const dbUsername = "root";
const dbPassword = "root";

app.use(bodyParser.json());

app.use(function(req, res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");
    next();
});

app.get('/test', function(req, res) {
    res.send("This is a test message")
})

app.get('/lists/:userid', function(req,res){
    console.log('In GET /lists route for ' + req.params.userid);
    let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);
    console.log("dao has been created");
    dao.findUsersLists(req.params.userid, function(lists){
        res.json(lists);
    });
});


app.get('/lists/:listid/tasks/', function(req,res){
    console.log('In GET /lists/tasks route for ' + req.params.listid);
    let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);

    dao.findListTasks(req.params.listid, function (tasks){
        res.json(tasks);
    });
});

app.post('/authenticate', function(req,  res){
    console.log("In /authenticate with post of " + JSON.stringify(req.body));
    if(!req.body.username.length){
        res.statusCode(400).json({error: "Invalid user posted"});
    } else {
        let user = new User(req.body.username, req.body.emailaddress, req.body.password);

        let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);
        dao.authenticate(user, function(userid){
            res.status(200).json({"userid": userid});
        })
    }
});

app.post('/users', function(req,res){
    console.log('In POST /users with post of ' + JSON.stringify(req.body));
    if(!req.body.username.length){
        res.statusCode(400).json({error: "Invalid User Posted"});
    } else {
        let user = new User(req.body.username, req.body.emailaddress, req.body.password);

        let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);
        dao.createUser(user, function(userId){
            if(userId == -1){
                res.status(200).json({"message": "Failed to create user"});
            } else {
                res.status(200).json({"message": "User created with id of " + userId});
            }
        })
    }
});

app.put('/users', function(req,res){
    console.log('In PUT /users with post of ' + JSON.stringify(req.body));
    if(!req.body.username){
        res.statusCode(400).json({error: "Invalid User Posted"});
    } else {
        let user = new User(req.body.username, req.body.emailaddress, req.body.password, req.body.userid);

        let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);
        dao.updateUser(user, function(returncode){
            if(returncode == -1){
                res.status(200).json({"error": "Update user failed"});
            } else {
                res.status(200).json({"message": "Update user was successful"});
            }
        })
    }
});

app.delete('/users/:userid', function(req,res){
    console.log("In DELETE /users route with id of " + req.params.userid);
    let userid = Number(req.params.userid);

    let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);
    dao.deleteUser(userid, function(returncode){
        if(returncode == -1){
            res.status(200).json({"error": "Delete user failed"});
        } else {
            res.status(200).json({"message": "Delete user was successful"});
        }
    });
});

app.post('/lists', function(req,res){
    console.log('In POST /lists with post of ' + JSON.stringify(req.body));
    if(!req.body.listname){
        res.statusCode(400).json({error: "Invalid List Posted"});
    } else {
        let list = new List(req.body.userid, req.body.listname);

        let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);
        dao.createList(list, function(listId){
            if(listId == -1){
                res.status(200).json({"message": "Creating List failed"});
            } else {
                res.status(200).json({"message": "List created with id of " + listId});
            }
        })
    }
});

app.put('/lists', function(req,res){
    console.log('In PUT /lists with post of ' + JSON.stringify(req.body));
    if(!req.body.listname){
        res.statusCode(400).json({error: "Invalid List Posted"});
    } else {
        let list = new List(req.body.userid, req.body.listname, req.listid);

        let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);
        dao.updateList(list, function(returncode){
            if(returncode == -1){
                res.status(200).json({"error": "Update list failed"});
            } else {
                res.status(200).json({"message": "List update successfully"});
            }
        })
    }
});

app.delete('/lists/:listid', function(req,res){
    console.log("In DELETE /lists route with id of " + req.params.listid);
    let listid = Number(req.params.listid);

    let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);
    dao.deleteList(listid, function(returncode){
        if(returncode == -1){
            res.status(200).json({"message": "Delete list failed"});
        } else {
            res.status(200).json({"message": "Delete list was successful"});
        }
    });
});

app.post('/list/:listid/tasks', function(req,res){
    console.log('In POST /lists/:listid/tasks with post of ' + JSON.stringify(req.body));
    if(!req.body.name){
        res.statusCode(400).json({error: "Invalid Task Posted"});
    } else {
        let task = new Task(req.body.listid, req.body.name, req.body.description, req.body.dependencies, req.body.priority, req.body.status, req.body.duedate, req.body.completed);

        let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);
        dao.createTask(task, function(taskId){
            if(taskId == -1){
                res.status(200).json({"error": "Creating task failed"});
            } else {
                res.status(200).json({"message": "Task created with id of " + taskId});
            }
        })
    }
});

app.put('/list/:listid/tasks', function(req,res){
    console.log('In PUT /lists/:listid/tasks with post of ' + JSON.stringify(req.body));
    if(!req.body.name){
        res.statusCode(400).json({error: "Invalid Task Posted"});
    } else {
        let task = new Task(req.body.listid, req.body.name, req.body.description, req.body.dependencies, req.body.priority, req.body.status, req.body.duedate, req.body.completed, req.body.taskid);

        let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);
        dao.createTask(task, function(returncode){
            if(returncode == -1){
                res.status(200).json({"error": "Updating task failed"});
            } else {
                res.status(200).json({"message": "Task updated successfully"});
            }
        })
    }
});

app.delete('/lists/:listid/tasks/:taskid', function(req,res){
    console.log("In DELETE /lists/:listid/tasks route with id of " + req.params.taskid);
    let taskid = Number(req.params.taskid);

    let dao = new ListDAO(dbHost, dbPort, dbUsername, dbPassword);
    dao.deleteList(taskid, function(returncode){
        if(returncode == -1){
            res.status(200).json({"error": "Delete task failed"});
        } else {
            res.status(200).json({"message": "Delete task was successful"});
        }
    });
});

app.listen(port, () =>{
    console.log(`Listening on port ${port}!`);
})