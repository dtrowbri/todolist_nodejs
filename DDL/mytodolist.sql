CREATE DATABASE `mytodolist` /*!40100 DEFAULT CHARACTER SET utf8 */;
CREATE TABLE `lists` (
  `listid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `listname` varchar(50) NOT NULL,
  PRIMARY KEY (`listid`),
  UNIQUE KEY `listid_UNIQUE` (`listid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `tasks` (
  `taskid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `listid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `dependencies` varchar(8000) NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `duedate` date NOT NULL,
  `completed` bit(1) NOT NULL,
  PRIMARY KEY (`taskid`),
  UNIQUE KEY `taskid_UNIQUE` (`taskid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `users` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `emailaddress` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `userid_UNIQUE` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
