"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.User = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var User = /*#__PURE__*/function () {
  function User(username, emailaddress, password, userid) {
    (0, _classCallCheck2.default)(this, User);
    (0, _defineProperty2.default)(this, "userid", void 0);
    (0, _defineProperty2.default)(this, "username", void 0);
    (0, _defineProperty2.default)(this, "emailaddress", void 0);
    (0, _defineProperty2.default)(this, "password", void 0);
    this.username = username;
    this.emailaddress = emailaddress;
    this.password = password;

    if (userid) {
      this.userid = userid;
    } else {
      this.userid = -1;
    }
  }

  (0, _createClass2.default)(User, [{
    key: "UserId",
    get: function get() {
      return this.UserId;
    },
    set: function set(userid) {
      this.userid = userid;
    }
  }, {
    key: "UserName",
    get: function get() {
      return this.username;
    },
    set: function set(username) {
      this.username = username;
    }
  }, {
    key: "EmailAddress",
    get: function get() {
      return this.emailaddress;
    },
    set: function set(emailaddress) {
      this.emailaddress = emailaddress;
    }
  }, {
    key: "Password",
    get: function get() {
      return this.password;
    },
    set: function set(password) {
      this.password = password;
    }
  }]);
  return User;
}();

exports.User = User;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJVc2VyIiwidXNlcm5hbWUiLCJlbWFpbGFkZHJlc3MiLCJwYXNzd29yZCIsInVzZXJpZCIsIlVzZXJJZCJdLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2FwcC9tb2RlbHMvVXNlci50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgVXNlciB7XHJcblxyXG4gICAgcHJpdmF0ZSB1c2VyaWQ6IG51bWJlcjtcclxuICAgIHByaXZhdGUgdXNlcm5hbWU6IHN0cmluZztcclxuICAgIHByaXZhdGUgZW1haWxhZGRyZXNzOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIHBhc3N3b3JkOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IodXNlcm5hbWU6c3RyaW5nLCBlbWFpbGFkZHJlc3M6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZywgdXNlcmlkPyA6IG51bWJlcil7XHJcbiAgICAgICAgdGhpcy51c2VybmFtZSA9IHVzZXJuYW1lO1xyXG4gICAgICAgIHRoaXMuZW1haWxhZGRyZXNzID0gZW1haWxhZGRyZXNzO1xyXG4gICAgICAgIHRoaXMucGFzc3dvcmQgPSBwYXNzd29yZDtcclxuXHJcbiAgICAgICAgaWYodXNlcmlkKXtcclxuICAgICAgICAgICAgdGhpcy51c2VyaWQgPSB1c2VyaWQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy51c2VyaWQgPSAtMTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IFVzZXJJZCgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLlVzZXJJZDtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgVXNlcklkKHVzZXJpZDogbnVtYmVyKXtcclxuICAgICAgICB0aGlzLnVzZXJpZCA9IHVzZXJpZDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgVXNlck5hbWUoKTpzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnVzZXJuYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBVc2VyTmFtZSh1c2VybmFtZTogc3RyaW5nKXtcclxuICAgICAgICB0aGlzLnVzZXJuYW1lID0gdXNlcm5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IEVtYWlsQWRkcmVzcygpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVtYWlsYWRkcmVzcztcclxuICAgIH1cclxuXHJcbiAgICBzZXQgRW1haWxBZGRyZXNzKGVtYWlsYWRkcmVzczogc3RyaW5nKXtcclxuICAgICAgICB0aGlzLmVtYWlsYWRkcmVzcyA9IGVtYWlsYWRkcmVzcztcclxuICAgIH1cclxuXHJcbiAgICBnZXQgUGFzc3dvcmQoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wYXNzd29yZDtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgUGFzc3dvcmQocGFzc3dvcmQ6IHN0cmluZyl7XHJcbiAgICAgICAgdGhpcy5wYXNzd29yZCA9IHBhc3N3b3JkO1xyXG4gICAgfVxyXG5cclxufSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0lBQWFBLEk7RUFPVCxjQUFZQyxRQUFaLEVBQTZCQyxZQUE3QixFQUFtREMsUUFBbkQsRUFBcUVDLE1BQXJFLEVBQXNGO0lBQUE7SUFBQTtJQUFBO0lBQUE7SUFBQTtJQUNsRixLQUFLSCxRQUFMLEdBQWdCQSxRQUFoQjtJQUNBLEtBQUtDLFlBQUwsR0FBb0JBLFlBQXBCO0lBQ0EsS0FBS0MsUUFBTCxHQUFnQkEsUUFBaEI7O0lBRUEsSUFBR0MsTUFBSCxFQUFVO01BQ04sS0FBS0EsTUFBTCxHQUFjQSxNQUFkO0lBQ0gsQ0FGRCxNQUVPO01BQ0gsS0FBS0EsTUFBTCxHQUFjLENBQUMsQ0FBZjtJQUNIO0VBQ0o7Ozs7U0FFRCxlQUFxQjtNQUNqQixPQUFPLEtBQUtDLE1BQVo7SUFDSCxDO1NBRUQsYUFBV0QsTUFBWCxFQUEwQjtNQUN0QixLQUFLQSxNQUFMLEdBQWNBLE1BQWQ7SUFDSDs7O1NBRUQsZUFBc0I7TUFDbEIsT0FBTyxLQUFLSCxRQUFaO0lBQ0gsQztTQUVELGFBQWFBLFFBQWIsRUFBOEI7TUFDMUIsS0FBS0EsUUFBTCxHQUFnQkEsUUFBaEI7SUFDSDs7O1NBRUQsZUFBMkI7TUFDdkIsT0FBTyxLQUFLQyxZQUFaO0lBQ0gsQztTQUVELGFBQWlCQSxZQUFqQixFQUFzQztNQUNsQyxLQUFLQSxZQUFMLEdBQW9CQSxZQUFwQjtJQUNIOzs7U0FFRCxlQUF1QjtNQUNuQixPQUFPLEtBQUtDLFFBQVo7SUFDSCxDO1NBRUQsYUFBYUEsUUFBYixFQUE4QjtNQUMxQixLQUFLQSxRQUFMLEdBQWdCQSxRQUFoQjtJQUNIIn0=