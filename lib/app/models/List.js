"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.List = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var List = /*#__PURE__*/function () {
  function List(userid, listname, listid) {
    (0, _classCallCheck2.default)(this, List);
    (0, _defineProperty2.default)(this, "listid", void 0);
    (0, _defineProperty2.default)(this, "userid", void 0);
    (0, _defineProperty2.default)(this, "listname", void 0);
    this.userid = userid;
    this.listname = listname;

    if (listid) {
      this.listid = listid;
    } else {
      this.listid = -1;
    }
  }

  (0, _createClass2.default)(List, [{
    key: "ListId",
    get: function get() {
      return this.ListId;
    },
    set: function set(listid) {
      this.listid = listid;
    }
  }, {
    key: "UserId",
    get: function get() {
      return this.userid;
    },
    set: function set(userid) {
      this.userid = userid;
    }
  }, {
    key: "ListName",
    get: function get() {
      return this.listname;
    },
    set: function set(listname) {
      this.listname = listname;
    }
  }]);
  return List;
}();

exports.List = List;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJMaXN0IiwidXNlcmlkIiwibGlzdG5hbWUiLCJsaXN0aWQiLCJMaXN0SWQiXSwic291cmNlcyI6WyIuLi8uLi8uLi9hcHAvbW9kZWxzL0xpc3QudHMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIExpc3Qge1xyXG5cclxuICAgIHByaXZhdGUgbGlzdGlkOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIHVzZXJpZDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBsaXN0bmFtZTogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHVzZXJpZDogbnVtYmVyLCBsaXN0bmFtZTogc3RyaW5nLCBsaXN0aWQ/IDogbnVtYmVyKXtcclxuICAgICAgICB0aGlzLnVzZXJpZCA9IHVzZXJpZDtcclxuICAgICAgICB0aGlzLmxpc3RuYW1lID0gbGlzdG5hbWU7XHJcblxyXG4gICAgICAgIGlmKGxpc3RpZCl7XHJcbiAgICAgICAgICAgIHRoaXMubGlzdGlkID0gbGlzdGlkO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubGlzdGlkID0gLTE7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBMaXN0SWQoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5MaXN0SWQ7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IExpc3RJZChsaXN0aWQ6IG51bWJlcil7XHJcbiAgICAgICAgdGhpcy5saXN0aWQgPSBsaXN0aWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IFVzZXJJZCgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnVzZXJpZDtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgVXNlcklkKHVzZXJpZDogbnVtYmVyKXtcclxuICAgICAgICB0aGlzLnVzZXJpZCA9IHVzZXJpZDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgTGlzdE5hbWUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5saXN0bmFtZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgTGlzdE5hbWUobGlzdG5hbWU6IHN0cmluZyl7XHJcbiAgICAgICAgdGhpcy5saXN0bmFtZSA9IGxpc3RuYW1lO1xyXG4gICAgfVxyXG59Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7SUFBYUEsSTtFQU1ULGNBQVlDLE1BQVosRUFBNEJDLFFBQTVCLEVBQThDQyxNQUE5QyxFQUErRDtJQUFBO0lBQUE7SUFBQTtJQUFBO0lBQzNELEtBQUtGLE1BQUwsR0FBY0EsTUFBZDtJQUNBLEtBQUtDLFFBQUwsR0FBZ0JBLFFBQWhCOztJQUVBLElBQUdDLE1BQUgsRUFBVTtNQUNOLEtBQUtBLE1BQUwsR0FBY0EsTUFBZDtJQUNILENBRkQsTUFFTztNQUNILEtBQUtBLE1BQUwsR0FBYyxDQUFDLENBQWY7SUFDSDtFQUNKOzs7O1NBRUQsZUFBcUI7TUFDakIsT0FBTyxLQUFLQyxNQUFaO0lBQ0gsQztTQUVELGFBQVdELE1BQVgsRUFBMEI7TUFDdEIsS0FBS0EsTUFBTCxHQUFjQSxNQUFkO0lBQ0g7OztTQUVELGVBQXFCO01BQ2pCLE9BQU8sS0FBS0YsTUFBWjtJQUNILEM7U0FFRCxhQUFXQSxNQUFYLEVBQTBCO01BQ3RCLEtBQUtBLE1BQUwsR0FBY0EsTUFBZDtJQUNIOzs7U0FFRCxlQUF1QjtNQUNuQixPQUFPLEtBQUtDLFFBQVo7SUFDSCxDO1NBRUQsYUFBYUEsUUFBYixFQUE4QjtNQUMxQixLQUFLQSxRQUFMLEdBQWdCQSxRQUFoQjtJQUNIIn0=