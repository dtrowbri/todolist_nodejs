"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Task = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var Task = /*#__PURE__*/function () {
  function Task(listid, name, description, dependencies, priority, status, duedate, completed, taskid) {
    (0, _classCallCheck2.default)(this, Task);
    (0, _defineProperty2.default)(this, "taskid", void 0);
    (0, _defineProperty2.default)(this, "listid", void 0);
    (0, _defineProperty2.default)(this, "name", void 0);
    (0, _defineProperty2.default)(this, "description", void 0);
    (0, _defineProperty2.default)(this, "dependencies", void 0);
    (0, _defineProperty2.default)(this, "priority", void 0);
    (0, _defineProperty2.default)(this, "status", void 0);
    (0, _defineProperty2.default)(this, "duedate", void 0);
    (0, _defineProperty2.default)(this, "completed", void 0);
    this.listid = listid;
    this.name = name;
    this.description = description;
    this.dependencies = dependencies;
    this.priority = priority;
    this.status = status;
    this.duedate = duedate;
    this.completed = completed;

    if (taskid) {
      this.taskid = taskid;
    } else {
      this.taskid = -1;
    }
  }

  (0, _createClass2.default)(Task, [{
    key: "TaskId",
    get: function get() {
      return this.taskid;
    },
    set: function set(taskid) {
      this.taskid = taskid;
    }
  }, {
    key: "ListId",
    get: function get() {
      return this.listid;
    },
    set: function set(listid) {
      this.listid = listid;
    }
  }, {
    key: "Name",
    get: function get() {
      return this.name;
    },
    set: function set(name) {
      this.name = name;
    }
  }, {
    key: "Description",
    get: function get() {
      return this.description;
    },
    set: function set(description) {
      this.description = description;
    }
  }, {
    key: "Dependencies",
    get: function get() {
      return this.dependencies;
    },
    set: function set(dependencies) {
      this.dependencies = dependencies;
    }
  }, {
    key: "Priority",
    get: function get() {
      return this.priority;
    },
    set: function set(priority) {
      this.priority = priority;
    }
  }, {
    key: "Status",
    get: function get() {
      return this.status;
    },
    set: function set(status) {
      this.status = status;
    }
  }, {
    key: "DueDate",
    get: function get() {
      return this.DueDate;
    },
    set: function set(duedate) {
      this.duedate = duedate;
    }
  }, {
    key: "Completed",
    get: function get() {
      return this.completed;
    },
    set: function set(completed) {
      this.completed = completed;
    }
  }]);
  return Task;
}();

exports.Task = Task;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJUYXNrIiwibGlzdGlkIiwibmFtZSIsImRlc2NyaXB0aW9uIiwiZGVwZW5kZW5jaWVzIiwicHJpb3JpdHkiLCJzdGF0dXMiLCJkdWVkYXRlIiwiY29tcGxldGVkIiwidGFza2lkIiwiRHVlRGF0ZSJdLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2FwcC9tb2RlbHMvVGFzay50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgVGFzayB7XHJcblxyXG4gICAgcHJpdmF0ZSB0YXNraWQ6IG51bWJlcjtcclxuICAgIHByaXZhdGUgbGlzdGlkOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIG5hbWU6IHN0cmluZztcclxuICAgIHByaXZhdGUgZGVzY3JpcHRpb246IHN0cmluZztcclxuICAgIHByaXZhdGUgZGVwZW5kZW5jaWVzOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIHByaW9yaXR5OiBudW1iZXI7XHJcbiAgICBwcml2YXRlIHN0YXR1czogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBkdWVkYXRlOiBEYXRlO1xyXG4gICAgcHJpdmF0ZSBjb21wbGV0ZWQ6IGJvb2xlYW47XHJcblxyXG4gICAgY29uc3RydWN0b3IobGlzdGlkOiBudW1iZXIsIG5hbWU6IHN0cmluZywgZGVzY3JpcHRpb246IHN0cmluZywgZGVwZW5kZW5jaWVzOiBzdHJpbmcsIHByaW9yaXR5OiBudW1iZXIsIHN0YXR1czogbnVtYmVyLCBkdWVkYXRlOiBEYXRlLCBjb21wbGV0ZWQ6IGJvb2xlYW4sIHRhc2tpZD8gOiBudW1iZXIpe1xyXG4gICAgICAgIHRoaXMubGlzdGlkID0gbGlzdGlkO1xyXG4gICAgICAgIHRoaXMubmFtZSA9IG5hbWU7XHJcbiAgICAgICAgdGhpcy5kZXNjcmlwdGlvbiA9IGRlc2NyaXB0aW9uO1xyXG4gICAgICAgIHRoaXMuZGVwZW5kZW5jaWVzID0gZGVwZW5kZW5jaWVzO1xyXG4gICAgICAgIHRoaXMucHJpb3JpdHkgPSBwcmlvcml0eTtcclxuICAgICAgICB0aGlzLnN0YXR1cyA9IHN0YXR1cztcclxuICAgICAgICB0aGlzLmR1ZWRhdGUgPSBkdWVkYXRlO1xyXG4gICAgICAgIHRoaXMuY29tcGxldGVkID0gY29tcGxldGVkO1xyXG5cclxuICAgICAgICBpZih0YXNraWQpe1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tpZCA9IHRhc2tpZDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tpZCA9IC0xO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXQgVGFza0lkKCk6bnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50YXNraWQ7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IFRhc2tJZCh0YXNraWQ6IG51bWJlcil7XHJcbiAgICAgICAgdGhpcy50YXNraWQgPSB0YXNraWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IExpc3RJZCgpOm51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMubGlzdGlkO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBMaXN0SWQobGlzdGlkOiBudW1iZXIpe1xyXG4gICAgICAgIHRoaXMubGlzdGlkID0gbGlzdGlkO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBOYW1lKCk6c3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5uYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBOYW1lKG5hbWU6IHN0cmluZyl7XHJcbiAgICAgICAgdGhpcy5uYW1lID0gbmFtZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgRGVzY3JpcHRpb24oKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5kZXNjcmlwdGlvbjtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgRGVzY3JpcHRpb24oZGVzY3JpcHRpb246IHN0cmluZyl7XHJcbiAgICAgICAgdGhpcy5kZXNjcmlwdGlvbiA9IGRlc2NyaXB0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBEZXBlbmRlbmNpZXMoKTogc3RyaW5ne1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRlcGVuZGVuY2llcztcclxuICAgIH1cclxuXHJcbiAgICBzZXQgRGVwZW5kZW5jaWVzKGRlcGVuZGVuY2llczogc3RyaW5nKXtcclxuICAgICAgICB0aGlzLmRlcGVuZGVuY2llcyA9IGRlcGVuZGVuY2llcztcclxuICAgIH1cclxuXHJcbiAgICBnZXQgUHJpb3JpdHkoKTpudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByaW9yaXR5O1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBQcmlvcml0eShwcmlvcml0eTogbnVtYmVyKXtcclxuICAgICAgICB0aGlzLnByaW9yaXR5ID0gcHJpb3JpdHk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IFN0YXR1cygpOm51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhdHVzO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBTdGF0dXMoc3RhdHVzOiBudW1iZXIpe1xyXG4gICAgICAgIHRoaXMuc3RhdHVzID0gc3RhdHVzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBEdWVEYXRlKCk6RGF0ZSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuRHVlRGF0ZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgRHVlRGF0ZShkdWVkYXRlOkRhdGUpe1xyXG4gICAgICAgIHRoaXMuZHVlZGF0ZSA9IGR1ZWRhdGU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IENvbXBsZXRlZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jb21wbGV0ZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IENvbXBsZXRlZChjb21wbGV0ZWQ6IGJvb2xlYW4pe1xyXG4gICAgICAgIHRoaXMuY29tcGxldGVkID0gY29tcGxldGVkO1xyXG4gICAgfVxyXG59Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7SUFBYUEsSTtFQVlULGNBQVlDLE1BQVosRUFBNEJDLElBQTVCLEVBQTBDQyxXQUExQyxFQUErREMsWUFBL0QsRUFBcUZDLFFBQXJGLEVBQXVHQyxNQUF2RyxFQUF1SEMsT0FBdkgsRUFBc0lDLFNBQXRJLEVBQTBKQyxNQUExSixFQUEySztJQUFBO0lBQUE7SUFBQTtJQUFBO0lBQUE7SUFBQTtJQUFBO0lBQUE7SUFBQTtJQUFBO0lBQ3ZLLEtBQUtSLE1BQUwsR0FBY0EsTUFBZDtJQUNBLEtBQUtDLElBQUwsR0FBWUEsSUFBWjtJQUNBLEtBQUtDLFdBQUwsR0FBbUJBLFdBQW5CO0lBQ0EsS0FBS0MsWUFBTCxHQUFvQkEsWUFBcEI7SUFDQSxLQUFLQyxRQUFMLEdBQWdCQSxRQUFoQjtJQUNBLEtBQUtDLE1BQUwsR0FBY0EsTUFBZDtJQUNBLEtBQUtDLE9BQUwsR0FBZUEsT0FBZjtJQUNBLEtBQUtDLFNBQUwsR0FBaUJBLFNBQWpCOztJQUVBLElBQUdDLE1BQUgsRUFBVTtNQUNOLEtBQUtBLE1BQUwsR0FBY0EsTUFBZDtJQUNILENBRkQsTUFFTztNQUNILEtBQUtBLE1BQUwsR0FBYyxDQUFDLENBQWY7SUFDSDtFQUNKOzs7O1NBRUQsZUFBb0I7TUFDaEIsT0FBTyxLQUFLQSxNQUFaO0lBQ0gsQztTQUVELGFBQVdBLE1BQVgsRUFBMEI7TUFDdEIsS0FBS0EsTUFBTCxHQUFjQSxNQUFkO0lBQ0g7OztTQUVELGVBQW9CO01BQ2hCLE9BQU8sS0FBS1IsTUFBWjtJQUNILEM7U0FFRCxhQUFXQSxNQUFYLEVBQTBCO01BQ3RCLEtBQUtBLE1BQUwsR0FBY0EsTUFBZDtJQUNIOzs7U0FFRCxlQUFrQjtNQUNkLE9BQU8sS0FBS0MsSUFBWjtJQUNILEM7U0FFRCxhQUFTQSxJQUFULEVBQXNCO01BQ2xCLEtBQUtBLElBQUwsR0FBWUEsSUFBWjtJQUNIOzs7U0FFRCxlQUEwQjtNQUN0QixPQUFPLEtBQUtDLFdBQVo7SUFDSCxDO1NBRUQsYUFBZ0JBLFdBQWhCLEVBQW9DO01BQ2hDLEtBQUtBLFdBQUwsR0FBbUJBLFdBQW5CO0lBQ0g7OztTQUVELGVBQTBCO01BQ3RCLE9BQU8sS0FBS0MsWUFBWjtJQUNILEM7U0FFRCxhQUFpQkEsWUFBakIsRUFBc0M7TUFDbEMsS0FBS0EsWUFBTCxHQUFvQkEsWUFBcEI7SUFDSDs7O1NBRUQsZUFBc0I7TUFDbEIsT0FBTyxLQUFLQyxRQUFaO0lBQ0gsQztTQUVELGFBQWFBLFFBQWIsRUFBOEI7TUFDMUIsS0FBS0EsUUFBTCxHQUFnQkEsUUFBaEI7SUFDSDs7O1NBRUQsZUFBb0I7TUFDaEIsT0FBTyxLQUFLQyxNQUFaO0lBQ0gsQztTQUVELGFBQVdBLE1BQVgsRUFBMEI7TUFDdEIsS0FBS0EsTUFBTCxHQUFjQSxNQUFkO0lBQ0g7OztTQUVELGVBQW1CO01BQ2YsT0FBTyxLQUFLSSxPQUFaO0lBQ0gsQztTQUVELGFBQVlILE9BQVosRUFBeUI7TUFDckIsS0FBS0EsT0FBTCxHQUFlQSxPQUFmO0lBQ0g7OztTQUVELGVBQXlCO01BQ3JCLE9BQU8sS0FBS0MsU0FBWjtJQUNILEM7U0FFRCxhQUFjQSxTQUFkLEVBQWlDO01BQzdCLEtBQUtBLFNBQUwsR0FBaUJBLFNBQWpCO0lBQ0gifQ==