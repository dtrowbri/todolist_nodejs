"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _typeof = require("@babel/runtime/helpers/typeof");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ListDAO = void 0;

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _List = require("../models/List");

var _Task = require("../models/Task");

var mysql = _interopRequireWildcard(require("mysql"));

var util = _interopRequireWildcard(require("util"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return generator._invoke = function (innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; }(innerFn, self, context), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; this._invoke = function (method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); }; } function maybeInvokeDelegate(delegate, context) { var method = delegate.iterator[context.method]; if (undefined === method) { if (context.delegate = null, "throw" === context.method) { if (delegate.iterator.return && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method)) return ContinueSentinel; context.method = "throw", context.arg = new TypeError("The iterator does not provide a 'throw' method"); } return ContinueSentinel; } var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) { if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; } return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, define(Gp, "constructor", GeneratorFunctionPrototype), define(GeneratorFunctionPrototype, "constructor", GeneratorFunction), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (object) { var keys = []; for (var key in object) { keys.push(key); } return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) { "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); } }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, catch: function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }

var ListDAO = /*#__PURE__*/function () {
  function ListDAO(host, port, username, password) {
    (0, _classCallCheck2.default)(this, ListDAO);
    (0, _defineProperty2.default)(this, "host", "");
    (0, _defineProperty2.default)(this, "port", 3306);
    (0, _defineProperty2.default)(this, "username", "");
    (0, _defineProperty2.default)(this, "password", "");
    (0, _defineProperty2.default)(this, "schema", "mytodolist");
    (0, _defineProperty2.default)(this, "pool", this.initDbConnection());
    this.host = host;
    this.port = port;
    this.username = username;
    this.password = password;
    this.pool = this.initDbConnection();
  }

  (0, _createClass2.default)(ListDAO, [{
    key: "initDbConnection",
    value: function initDbConnection() {
      return mysql.createPool({
        host: this.host,
        port: this.port,
        user: this.username,
        password: this.password,
        database: this.schema,
        connectionLimit: 10
      });
    }
  }, {
    key: "findUsersLists",
    value: function findUsersLists(userid, callback) {
      var lists = [];
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(err, connection) {
          var results, x;
          return _regeneratorRuntime().wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  connection.release();

                  if (!err) {
                    _context.next = 3;
                    break;
                  }

                  throw err;

                case 3:
                  connection.query = util.promisify(connection.query);
                  console.log(userid);
                  _context.next = 7;
                  return connection.query('SELECT * FROM LISTS WHERE USERID = ?', [userid]);

                case 7:
                  results = _context.sent;
                  console.log(results);

                  for (x = 0; x < results.length; x++) {
                    lists.push(new _List.List(userid, results[x].listname, results[x].listid));
                  }

                  callback(lists);

                case 11:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }));

        return function (_x, _x2) {
          return _ref.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "authenticate",
    value: function authenticate(user, callback) {
      var userid = -1;
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref2 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(err, connection) {
          var results;
          return _regeneratorRuntime().wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  connection.release();

                  if (!err) {
                    _context2.next = 3;
                    break;
                  }

                  throw err;

                case 3:
                  connection.query = util.promisify(connection.query);
                  _context2.next = 6;
                  return connection.query("SELECT USERID FROM USERS WHERE USERNAME = ? AND PASSWORD = ?", [user.UserName, user.Password]);

                case 6:
                  results = _context2.sent;

                  if (results.length == 1) {
                    userid = results[0].USERID;
                  }

                  callback(userid);

                case 9:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2);
        }));

        return function (_x3, _x4) {
          return _ref2.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "findListTasks",
    value: function findListTasks(listid, callback) {
      var tasks = [];
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref3 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(err, connection) {
          var results, x;
          return _regeneratorRuntime().wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  connection.release();

                  if (!err) {
                    _context3.next = 3;
                    break;
                  }

                  throw err;

                case 3:
                  connection.query = util.promisify(connection.query);
                  _context3.next = 6;
                  return connection.query("SELECT * FROM TASKS WHERE LISTID = ?", [listid]);

                case 6:
                  results = _context3.sent;

                  for (x = 0; x < results.length; x++) {
                    tasks.push(new _Task.Task(listid, results[x].name, results[x].description, results[x].dependencies, results[x].priority, results[x].status, results[x].duedate, results[x].completed, results[x].taskid));
                  }

                  callback(tasks);

                case 9:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3);
        }));

        return function (_x5, _x6) {
          return _ref3.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "createUser",
    value: function createUser(user, callback) {
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref4 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4(err, connection) {
          var results;
          return _regeneratorRuntime().wrap(function _callee4$(_context4) {
            while (1) {
              switch (_context4.prev = _context4.next) {
                case 0:
                  connection.release();

                  if (!err) {
                    _context4.next = 3;
                    break;
                  }

                  throw err;

                case 3:
                  connection.query = util.promisify(connection.query);
                  _context4.next = 6;
                  return connection.query('INSERT INTO USERS (USERNAME, EMAILADDRESS, PASSWORD) VALUES(?,?,?)', [user.UserName, user.EmailAddress, user.Password]);

                case 6:
                  results = _context4.sent;

                  if (results.affectedRows != 1) {
                    callback(-1);
                  } else {
                    callback(results.insertId);
                  }

                case 8:
                case "end":
                  return _context4.stop();
              }
            }
          }, _callee4);
        }));

        return function (_x7, _x8) {
          return _ref4.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "updateUser",
    value: function updateUser(user, callback) {
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref5 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5(err, connection) {
          var results;
          return _regeneratorRuntime().wrap(function _callee5$(_context5) {
            while (1) {
              switch (_context5.prev = _context5.next) {
                case 0:
                  connection.release();

                  if (!err) {
                    _context5.next = 3;
                    break;
                  }

                  throw err;

                case 3:
                  connection.query = util.promisify(connection.query);
                  _context5.next = 6;
                  return connection.query("UPDATE USERS SET USERNAME = ?, EMAILADDRESS = ?, PASSWORD = ? WHERE USERID = ?", [user.UserName, user.EmailAddress, user.Password, user.UserId]);

                case 6:
                  results = _context5.sent;

                  if (results.affectedRows != 1) {
                    callback(-1);
                  } else {
                    callback(1);
                  }

                case 8:
                case "end":
                  return _context5.stop();
              }
            }
          }, _callee5);
        }));

        return function (_x9, _x10) {
          return _ref5.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "deleteUser",
    value: function deleteUser(userid, callback) {
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref6 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee6(err, connection) {
          var results;
          return _regeneratorRuntime().wrap(function _callee6$(_context6) {
            while (1) {
              switch (_context6.prev = _context6.next) {
                case 0:
                  connection.release();

                  if (!err) {
                    _context6.next = 3;
                    break;
                  }

                  throw err;

                case 3:
                  connection.query = util.promisify(connection.query);
                  _context6.next = 6;
                  return connection.query("DELETE FROM USERS WHERE USERID = ?", [userid]);

                case 6:
                  results = _context6.sent;

                  if (results.affectedRows != 1) {
                    callback(-1);
                  } else {
                    callback(1);
                  }

                case 8:
                case "end":
                  return _context6.stop();
              }
            }
          }, _callee6);
        }));

        return function (_x11, _x12) {
          return _ref6.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "createList",
    value: function createList(list, callback) {
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref7 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee7(err, connection) {
          var results;
          return _regeneratorRuntime().wrap(function _callee7$(_context7) {
            while (1) {
              switch (_context7.prev = _context7.next) {
                case 0:
                  connection.release();

                  if (!err) {
                    _context7.next = 3;
                    break;
                  }

                  throw err;

                case 3:
                  connection.query = util.promisify(connection.query);
                  _context7.next = 6;
                  return connection.query("INSERT INTO LISTS (USERID, LISTNAME) VALUES(?,?)", [list.UserId, list.ListName]);

                case 6:
                  results = _context7.sent;

                  if (results.affectedRows != 1) {
                    callback(-1);
                  } else {
                    callback(results.insertId);
                  }

                case 8:
                case "end":
                  return _context7.stop();
              }
            }
          }, _callee7);
        }));

        return function (_x13, _x14) {
          return _ref7.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "updateList",
    value: function updateList(list, callback) {
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref8 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee8(err, connection) {
          var results;
          return _regeneratorRuntime().wrap(function _callee8$(_context8) {
            while (1) {
              switch (_context8.prev = _context8.next) {
                case 0:
                  connection.release();

                  if (!err) {
                    _context8.next = 3;
                    break;
                  }

                  throw err;

                case 3:
                  connection.query = util.promisify(connection.query);
                  _context8.next = 6;
                  return connection.query("UPDATE LISTS SET LISTNAME = ? WHERE LISTID = ?", [list.ListId]);

                case 6:
                  results = _context8.sent;

                  if (results.affectedRows != 1) {
                    callback(-1);
                  } else {
                    callback(1);
                  }

                case 8:
                case "end":
                  return _context8.stop();
              }
            }
          }, _callee8);
        }));

        return function (_x15, _x16) {
          return _ref8.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "deleteList",
    value: function deleteList(listid, callback) {
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref9 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee9(err, connection) {
          var taskResults, listResults;
          return _regeneratorRuntime().wrap(function _callee9$(_context9) {
            while (1) {
              switch (_context9.prev = _context9.next) {
                case 0:
                  connection.release();
                  console.log("connection released");

                  if (!err) {
                    _context9.next = 4;
                    break;
                  }

                  throw err;

                case 4:
                  connection.query = util.promisify(connection.query);
                  _context9.next = 7;
                  return connection.query("DELETE FROM TASKS WHERE LISTID = ?", [listid]);

                case 7:
                  taskResults = _context9.sent;
                  console.log("task results ");
                  console.log(taskResults.affectedRows);

                  if (!(taskResults.affectedRows > -1)) {
                    _context9.next = 17;
                    break;
                  }

                  _context9.next = 13;
                  return connection.query("DELETE FROM LISTS WHERE LISTID = ?", [listid]);

                case 13:
                  listResults = _context9.sent;

                  if (listResults.affectedRows != 1) {
                    callback(-1);
                  } else {
                    callback(taskResults.affectedRows + listResults.affectedRows);
                  }

                  _context9.next = 18;
                  break;

                case 17:
                  callback(-1);

                case 18:
                case "end":
                  return _context9.stop();
              }
            }
          }, _callee9);
        }));

        return function (_x17, _x18) {
          return _ref9.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "createTask",
    value: function createTask(task, callback) {
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref10 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee10(err, connection) {
          var results;
          return _regeneratorRuntime().wrap(function _callee10$(_context10) {
            while (1) {
              switch (_context10.prev = _context10.next) {
                case 0:
                  connection.release();

                  if (!err) {
                    _context10.next = 3;
                    break;
                  }

                  throw err;

                case 3:
                  connection.query = util.promisify(connection.query);
                  _context10.next = 6;
                  return connection.query("INSERT INTO TASKS (LISTID, NAME, DESCRIPTION, DEPENDENCIES, PRIORITY, STATUS, DUEDATE, COMPLETED", [task.ListId, task.Name, task.Description, task.Dependencies, task.Priority, task.Status, task.DueDate, task.Completed]);

                case 6:
                  results = _context10.sent;

                  if (results.affectedRows != 1) {
                    callback(-1);
                  } else {
                    callback(results.insertId);
                  }

                case 8:
                case "end":
                  return _context10.stop();
              }
            }
          }, _callee10);
        }));

        return function (_x19, _x20) {
          return _ref10.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "updateTask",
    value: function updateTask(task, callback) {
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref11 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee11(err, connection) {
          var results;
          return _regeneratorRuntime().wrap(function _callee11$(_context11) {
            while (1) {
              switch (_context11.prev = _context11.next) {
                case 0:
                  connection.release();

                  if (!err) {
                    _context11.next = 3;
                    break;
                  }

                  throw err;

                case 3:
                  connection.query = util.promisify(connection.query);
                  _context11.next = 6;
                  return connection.query("UPDATE TASKS SET NAME = ?, DESCRIPTION = ?, DEPENDENCIES = ?, PRIORITY = ?, STATUS = ?, DUEDATE = ?, COMPLETED = ? WHERE TASKID = ?", [task.Name, task.Description, task.Dependencies, task.Priority, task.Status, task.DueDate, task.Completed, task.TaskId]);

                case 6:
                  results = _context11.sent;

                  if (results.affectedRows != 1) {
                    callback(-1);
                  } else {
                    callback(1);
                  }

                case 8:
                case "end":
                  return _context11.stop();
              }
            }
          }, _callee11);
        }));

        return function (_x21, _x22) {
          return _ref11.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "deleteTask",
    value: function deleteTask(taskid, callback) {
      this.pool.getConnection( /*#__PURE__*/function () {
        var _ref12 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regeneratorRuntime().mark(function _callee12(err, connection) {
          var results;
          return _regeneratorRuntime().wrap(function _callee12$(_context12) {
            while (1) {
              switch (_context12.prev = _context12.next) {
                case 0:
                  connection.release();

                  if (!err) {
                    _context12.next = 3;
                    break;
                  }

                  throw err;

                case 3:
                  connection.query = util.promisify(connection.query);
                  _context12.next = 6;
                  return connection.query("DELETE FROM TASKS WHERE TASKID = ?", [taskid]);

                case 6:
                  results = _context12.sent;

                  if (results.affectedRows != 1) {
                    callback(-1);
                  } else {
                    callback(1);
                  }

                case 8:
                case "end":
                  return _context12.stop();
              }
            }
          }, _callee12);
        }));

        return function (_x23, _x24) {
          return _ref12.apply(this, arguments);
        };
      }());
    }
  }]);
  return ListDAO;
}();

exports.ListDAO = ListDAO;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJMaXN0REFPIiwiaG9zdCIsInBvcnQiLCJ1c2VybmFtZSIsInBhc3N3b3JkIiwiaW5pdERiQ29ubmVjdGlvbiIsInBvb2wiLCJteXNxbCIsImNyZWF0ZVBvb2wiLCJ1c2VyIiwiZGF0YWJhc2UiLCJzY2hlbWEiLCJjb25uZWN0aW9uTGltaXQiLCJ1c2VyaWQiLCJjYWxsYmFjayIsImxpc3RzIiwiZ2V0Q29ubmVjdGlvbiIsImVyciIsImNvbm5lY3Rpb24iLCJyZWxlYXNlIiwicXVlcnkiLCJ1dGlsIiwicHJvbWlzaWZ5IiwiY29uc29sZSIsImxvZyIsInJlc3VsdHMiLCJ4IiwibGVuZ3RoIiwicHVzaCIsIkxpc3QiLCJsaXN0bmFtZSIsImxpc3RpZCIsIlVzZXJOYW1lIiwiUGFzc3dvcmQiLCJVU0VSSUQiLCJ0YXNrcyIsIlRhc2siLCJuYW1lIiwiZGVzY3JpcHRpb24iLCJkZXBlbmRlbmNpZXMiLCJwcmlvcml0eSIsInN0YXR1cyIsImR1ZWRhdGUiLCJjb21wbGV0ZWQiLCJ0YXNraWQiLCJFbWFpbEFkZHJlc3MiLCJhZmZlY3RlZFJvd3MiLCJpbnNlcnRJZCIsIlVzZXJJZCIsImxpc3QiLCJMaXN0TmFtZSIsIkxpc3RJZCIsInRhc2tSZXN1bHRzIiwibGlzdFJlc3VsdHMiLCJ0YXNrIiwiTmFtZSIsIkRlc2NyaXB0aW9uIiwiRGVwZW5kZW5jaWVzIiwiUHJpb3JpdHkiLCJTdGF0dXMiLCJEdWVEYXRlIiwiQ29tcGxldGVkIiwiVGFza0lkIl0sInNvdXJjZXMiOlsiLi4vLi4vLi4vYXBwL2RhdGFiYXNlL0xpc3REQU8udHMiXSwic291cmNlc0NvbnRlbnQiOlsiLypcclxuICAgIEF1dGhvcjogRG9uYWxkIFRyb3dicmlkZ2VcclxuICAgIE5vdGVzOiBzdGlsbCBkb24ndCBoYXZlIGEgd29ya2luZyBlbnZpcm9ubWVudC4gSSBoYXZlIG5vIGlkZWEgaWYgdGhlIGNvZGUgcnVucy5cclxuKi9cclxuXHJcbmltcG9ydCB7VXNlcn0gZnJvbSBcIi4uL21vZGVscy9Vc2VyXCI7XHJcbmltcG9ydCB7TGlzdH0gZnJvbSBcIi4uL21vZGVscy9MaXN0XCI7XHJcbmltcG9ydCB7IFRhc2sgfSBmcm9tIFwiLi4vbW9kZWxzL1Rhc2tcIjtcclxuaW1wb3J0ICogYXMgbXlzcWwgZnJvbSBcIm15c3FsXCI7XHJcbmltcG9ydCAqIGFzIHV0aWwgZnJvbSBcInV0aWxcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBMaXN0REFPe1xyXG5cclxuICAgIHByaXZhdGUgaG9zdDpzdHJpbmcgPSBcIlwiO1xyXG4gICAgcHJpdmF0ZSBwb3J0Om51bWJlciA9IDMzMDY7XHJcbiAgICBwcml2YXRlIHVzZXJuYW1lOnN0cmluZyA9IFwiXCI7XHJcbiAgICBwcml2YXRlIHBhc3N3b3JkOnN0cmluZyA9IFwiXCI7XHJcbiAgICBwcml2YXRlIHNjaGVtYTpzdHJpbmcgPSBcIm15dG9kb2xpc3RcIjtcclxuICAgIHByaXZhdGUgcG9vbCA9IHRoaXMuaW5pdERiQ29ubmVjdGlvbigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGhvc3Q6c3RyaW5nLCBwb3J0Om51bWJlciwgdXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZyl7XHJcbiAgICAgICAgdGhpcy5ob3N0ID0gaG9zdDtcclxuICAgICAgICB0aGlzLnBvcnQgPSBwb3J0O1xyXG4gICAgICAgIHRoaXMudXNlcm5hbWUgPSB1c2VybmFtZTtcclxuICAgICAgICB0aGlzLnBhc3N3b3JkID0gcGFzc3dvcmQ7XHJcbiAgICAgICAgdGhpcy5wb29sID0gdGhpcy5pbml0RGJDb25uZWN0aW9uKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBpbml0RGJDb25uZWN0aW9uKCk6YW55XHJcbiAgICB7XHJcbiAgICAgICAgcmV0dXJuIG15c3FsLmNyZWF0ZVBvb2woe2hvc3Q6IHRoaXMuaG9zdCwgcG9ydDogdGhpcy5wb3J0LCB1c2VyOiB0aGlzLnVzZXJuYW1lLCBwYXNzd29yZDogdGhpcy5wYXNzd29yZCwgZGF0YWJhc2U6IHRoaXMuc2NoZW1hLCBjb25uZWN0aW9uTGltaXQ6IDEwfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGZpbmRVc2Vyc0xpc3RzKHVzZXJpZDpudW1iZXIsIGNhbGxiYWNrOmFueSl7XHJcbiAgICAgICAgbGV0IGxpc3RzOkxpc3RbXSA9IFtdO1xyXG5cclxuICAgICAgICB0aGlzLnBvb2wuZ2V0Q29ubmVjdGlvbihhc3luYyBmdW5jdGlvbihlcnI6YW55LCBjb25uZWN0aW9uOmFueSl7XHJcbiAgICAgICAgICAgIGNvbm5lY3Rpb24ucmVsZWFzZSgpO1xyXG5cclxuICAgICAgICAgICAgaWYoZXJyKSB0aHJvdyBlcnI7XHJcblxyXG4gICAgICAgICAgICBjb25uZWN0aW9uLnF1ZXJ5ID0gdXRpbC5wcm9taXNpZnkoY29ubmVjdGlvbi5xdWVyeSk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHVzZXJpZCk7XHJcbiAgICAgICAgICAgIGxldCByZXN1bHRzID0gYXdhaXQgY29ubmVjdGlvbi5xdWVyeSgnU0VMRUNUICogRlJPTSBMSVNUUyBXSEVSRSBVU0VSSUQgPSA/JywgW3VzZXJpZF0pO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHRzKTtcclxuICAgICAgICAgICAgZm9yKGxldCB4ID0gMDsgeCA8IHJlc3VsdHMubGVuZ3RoOyB4Kyspe1xyXG4gICAgICAgICAgICAgICAgbGlzdHMucHVzaChuZXcgTGlzdCh1c2VyaWQsIHJlc3VsdHNbeF0ubGlzdG5hbWUsIHJlc3VsdHNbeF0ubGlzdGlkKSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNhbGxiYWNrKGxpc3RzKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXV0aGVudGljYXRlKHVzZXI6VXNlciwgY2FsbGJhY2s6YW55KXtcclxuICAgICAgICBsZXQgdXNlcmlkID0gLTE7XHJcblxyXG4gICAgICAgIHRoaXMucG9vbC5nZXRDb25uZWN0aW9uKGFzeW5jIGZ1bmN0aW9uKGVycjphbnksIGNvbm5lY3Rpb246YW55KXtcclxuICAgICAgICAgICAgY29ubmVjdGlvbi5yZWxlYXNlKCk7XHJcblxyXG4gICAgICAgICAgICBpZihlcnIpIHRocm93IGVycjtcclxuXHJcbiAgICAgICAgICAgIGNvbm5lY3Rpb24ucXVlcnkgPSB1dGlsLnByb21pc2lmeShjb25uZWN0aW9uLnF1ZXJ5KTtcclxuICAgICAgICAgICAgbGV0IHJlc3VsdHMgPSBhd2FpdCBjb25uZWN0aW9uLnF1ZXJ5KFwiU0VMRUNUIFVTRVJJRCBGUk9NIFVTRVJTIFdIRVJFIFVTRVJOQU1FID0gPyBBTkQgUEFTU1dPUkQgPSA/XCIsIFt1c2VyLlVzZXJOYW1lLCB1c2VyLlBhc3N3b3JkXSk7XHJcbiAgICAgICAgICAgIGlmKHJlc3VsdHMubGVuZ3RoID09IDEpe1xyXG4gICAgICAgICAgICAgICAgdXNlcmlkID0gcmVzdWx0c1swXS5VU0VSSUQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FsbGJhY2sodXNlcmlkKTtcclxuICAgICAgICB9KVxyXG4gICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBmaW5kTGlzdFRhc2tzKGxpc3RpZDpudW1iZXIsIGNhbGxiYWNrOmFueSl7XHJcbiAgICAgICAgbGV0IHRhc2tzOlRhc2tbXSA9IFtdO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMucG9vbC5nZXRDb25uZWN0aW9uKGFzeW5jIGZ1bmN0aW9uKGVycjphbnksIGNvbm5lY3Rpb246YW55KXtcclxuICAgICAgICAgICAgY29ubmVjdGlvbi5yZWxlYXNlKCk7XHJcblxyXG4gICAgICAgICAgICBpZihlcnIpIHRocm93IGVycjtcclxuXHJcbiAgICAgICAgICAgIGNvbm5lY3Rpb24ucXVlcnkgPSB1dGlsLnByb21pc2lmeShjb25uZWN0aW9uLnF1ZXJ5KTtcclxuICAgICAgICAgICAgbGV0IHJlc3VsdHMgPSBhd2FpdCBjb25uZWN0aW9uLnF1ZXJ5KFwiU0VMRUNUICogRlJPTSBUQVNLUyBXSEVSRSBMSVNUSUQgPSA/XCIsIFtsaXN0aWRdKTtcclxuICAgICAgICAgICAgZm9yKGxldCB4ID0gMDsgeCA8IHJlc3VsdHMubGVuZ3RoOyB4Kyspe1xyXG4gICAgICAgICAgICAgICAgdGFza3MucHVzaChuZXcgVGFzayhsaXN0aWQsIHJlc3VsdHNbeF0ubmFtZSwgcmVzdWx0c1t4XS5kZXNjcmlwdGlvbiwgcmVzdWx0c1t4XS5kZXBlbmRlbmNpZXMsIHJlc3VsdHNbeF0ucHJpb3JpdHksIHJlc3VsdHNbeF0uc3RhdHVzLCByZXN1bHRzW3hdLmR1ZWRhdGUsIHJlc3VsdHNbeF0uY29tcGxldGVkLCByZXN1bHRzW3hdLnRhc2tpZCkpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjYWxsYmFjayh0YXNrcyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNyZWF0ZVVzZXIodXNlcjpVc2VyLCBjYWxsYmFjazphbnkpe1xyXG4gICAgICAgIHRoaXMucG9vbC5nZXRDb25uZWN0aW9uKGFzeW5jIGZ1bmN0aW9uKGVycjphbnksIGNvbm5lY3Rpb246YW55KXtcclxuICAgICAgICAgICAgY29ubmVjdGlvbi5yZWxlYXNlKCk7XHJcblxyXG4gICAgICAgICAgICBpZihlcnIpIHRocm93IGVycjtcclxuXHJcbiAgICAgICAgICAgIGNvbm5lY3Rpb24ucXVlcnkgPSB1dGlsLnByb21pc2lmeShjb25uZWN0aW9uLnF1ZXJ5KTtcclxuICAgICAgICAgICAgbGV0IHJlc3VsdHMgPSBhd2FpdCBjb25uZWN0aW9uLnF1ZXJ5KCdJTlNFUlQgSU5UTyBVU0VSUyAoVVNFUk5BTUUsIEVNQUlMQUREUkVTUywgUEFTU1dPUkQpIFZBTFVFUyg/LD8sPyknLCBbdXNlci5Vc2VyTmFtZSwgdXNlci5FbWFpbEFkZHJlc3MsIHVzZXIuUGFzc3dvcmRdKTtcclxuICAgICAgICAgICAgaWYocmVzdWx0cy5hZmZlY3RlZFJvd3MgIT0gMSl7XHJcbiAgICAgICAgICAgICAgICBjYWxsYmFjaygtMSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjYWxsYmFjayhyZXN1bHRzLmluc2VydElkKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB1cGRhdGVVc2VyKHVzZXI6VXNlciwgY2FsbGJhY2s6YW55KXtcclxuICAgICAgICB0aGlzLnBvb2wuZ2V0Q29ubmVjdGlvbihhc3luYyBmdW5jdGlvbihlcnI6YW55LCBjb25uZWN0aW9uOiBhbnkpe1xyXG4gICAgICAgICAgICBjb25uZWN0aW9uLnJlbGVhc2UoKTtcclxuXHJcbiAgICAgICAgICAgIGlmKGVycikgdGhyb3cgZXJyO1xyXG5cclxuICAgICAgICAgICAgY29ubmVjdGlvbi5xdWVyeSA9IHV0aWwucHJvbWlzaWZ5KGNvbm5lY3Rpb24ucXVlcnkpO1xyXG4gICAgICAgICAgICBsZXQgcmVzdWx0cyA9IGF3YWl0IGNvbm5lY3Rpb24ucXVlcnkoXCJVUERBVEUgVVNFUlMgU0VUIFVTRVJOQU1FID0gPywgRU1BSUxBRERSRVNTID0gPywgUEFTU1dPUkQgPSA/IFdIRVJFIFVTRVJJRCA9ID9cIiwgW3VzZXIuVXNlck5hbWUsIHVzZXIuRW1haWxBZGRyZXNzLCB1c2VyLlBhc3N3b3JkLCB1c2VyLlVzZXJJZF0pO1xyXG4gICAgICAgICAgICBpZihyZXN1bHRzLmFmZmVjdGVkUm93cyAhPSAxKXtcclxuICAgICAgICAgICAgICAgIGNhbGxiYWNrKC0xKTtcclxuICAgICAgICAgICAgfWVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2soMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZGVsZXRlVXNlcih1c2VyaWQ6bnVtYmVyLCBjYWxsYmFjazogYW55KXtcclxuICAgICAgICB0aGlzLnBvb2wuZ2V0Q29ubmVjdGlvbihhc3luYyBmdW5jdGlvbihlcnI6YW55LCBjb25uZWN0aW9uOiBhbnkpe1xyXG4gICAgICAgICAgICBjb25uZWN0aW9uLnJlbGVhc2UoKTtcclxuXHJcbiAgICAgICAgICAgIGlmKGVycikgdGhyb3cgZXJyO1xyXG5cclxuICAgICAgICAgICAgY29ubmVjdGlvbi5xdWVyeSA9IHV0aWwucHJvbWlzaWZ5KGNvbm5lY3Rpb24ucXVlcnkpO1xyXG4gICAgICAgICAgICBsZXQgcmVzdWx0cyA9IGF3YWl0IGNvbm5lY3Rpb24ucXVlcnkoXCJERUxFVEUgRlJPTSBVU0VSUyBXSEVSRSBVU0VSSUQgPSA/XCIsIFt1c2VyaWRdKTtcclxuICAgICAgICAgICAgaWYocmVzdWx0cy5hZmZlY3RlZFJvd3MgIT0gMSl7XHJcbiAgICAgICAgICAgICAgICBjYWxsYmFjaygtMSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjYWxsYmFjaygxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBjcmVhdGVMaXN0KGxpc3Q6TGlzdCwgY2FsbGJhY2s6YW55KXtcclxuICAgICAgICB0aGlzLnBvb2wuZ2V0Q29ubmVjdGlvbihhc3luYyBmdW5jdGlvbihlcnI6YW55LCBjb25uZWN0aW9uOiBhbnkpe1xyXG4gICAgICAgICAgICBjb25uZWN0aW9uLnJlbGVhc2UoKTtcclxuXHJcbiAgICAgICAgICAgIGlmKGVycikgdGhyb3cgZXJyO1xyXG5cclxuICAgICAgICAgICAgY29ubmVjdGlvbi5xdWVyeSA9IHV0aWwucHJvbWlzaWZ5KGNvbm5lY3Rpb24ucXVlcnkpO1xyXG4gICAgICAgICAgICBsZXQgcmVzdWx0cyA9IGF3YWl0IGNvbm5lY3Rpb24ucXVlcnkoXCJJTlNFUlQgSU5UTyBMSVNUUyAoVVNFUklELCBMSVNUTkFNRSkgVkFMVUVTKD8sPylcIiwgW2xpc3QuVXNlcklkLCBsaXN0Lkxpc3ROYW1lXSk7XHJcbiAgICAgICAgICAgIGlmKHJlc3VsdHMuYWZmZWN0ZWRSb3dzICE9IDEpe1xyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2soLTEpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2socmVzdWx0cy5pbnNlcnRJZCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgdXBkYXRlTGlzdChsaXN0Okxpc3QsIGNhbGxiYWNrOmFueSl7XHJcbiAgICAgICAgdGhpcy5wb29sLmdldENvbm5lY3Rpb24oYXN5bmMgZnVuY3Rpb24oZXJyOmFueSwgY29ubmVjdGlvbjogYW55KXtcclxuICAgICAgICAgICAgY29ubmVjdGlvbi5yZWxlYXNlKCk7XHJcblxyXG4gICAgICAgICAgICBpZihlcnIpIHRocm93IGVycjtcclxuXHJcbiAgICAgICAgICAgIGNvbm5lY3Rpb24ucXVlcnkgPSB1dGlsLnByb21pc2lmeShjb25uZWN0aW9uLnF1ZXJ5KTtcclxuICAgICAgICAgICAgbGV0IHJlc3VsdHMgPSBhd2FpdCBjb25uZWN0aW9uLnF1ZXJ5KFwiVVBEQVRFIExJU1RTIFNFVCBMSVNUTkFNRSA9ID8gV0hFUkUgTElTVElEID0gP1wiLCBbbGlzdC5MaXN0SWRdKTtcclxuICAgICAgICAgICAgaWYocmVzdWx0cy5hZmZlY3RlZFJvd3MgIT0gMSl7XHJcbiAgICAgICAgICAgICAgICBjYWxsYmFjaygtMSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjYWxsYmFjaygxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBkZWxldGVMaXN0KGxpc3RpZDpudW1iZXIsIGNhbGxiYWNrOmFueSl7XHJcbiAgICAgICAgdGhpcy5wb29sLmdldENvbm5lY3Rpb24oYXN5bmMgZnVuY3Rpb24oZXJyOmFueSwgY29ubmVjdGlvbjogYW55KXtcclxuICAgICAgICAgICAgY29ubmVjdGlvbi5yZWxlYXNlKCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiY29ubmVjdGlvbiByZWxlYXNlZFwiKTtcclxuICAgICAgICAgICAgaWYoZXJyKSB0aHJvdyBlcnI7XHJcblxyXG4gICAgICAgICAgICBjb25uZWN0aW9uLnF1ZXJ5ID0gdXRpbC5wcm9taXNpZnkoY29ubmVjdGlvbi5xdWVyeSk7XHJcbiAgICAgICAgICAgIGxldCB0YXNrUmVzdWx0cyA9IGF3YWl0IGNvbm5lY3Rpb24ucXVlcnkoXCJERUxFVEUgRlJPTSBUQVNLUyBXSEVSRSBMSVNUSUQgPSA/XCIsIFtsaXN0aWRdKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJ0YXNrIHJlc3VsdHMgXCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0YXNrUmVzdWx0cy5hZmZlY3RlZFJvd3MpO1xyXG4gICAgICAgICAgICBpZih0YXNrUmVzdWx0cy5hZmZlY3RlZFJvd3MgPiAtMSl7XHJcbiAgICAgICAgICAgICAgICBsZXQgbGlzdFJlc3VsdHMgPSBhd2FpdCBjb25uZWN0aW9uLnF1ZXJ5KFwiREVMRVRFIEZST00gTElTVFMgV0hFUkUgTElTVElEID0gP1wiLCBbbGlzdGlkXSk7XHJcbiAgICAgICAgICAgICAgICBpZihsaXN0UmVzdWx0cy5hZmZlY3RlZFJvd3MgIT0gMSl7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soLTEpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh0YXNrUmVzdWx0cy5hZmZlY3RlZFJvd3MgKyBsaXN0UmVzdWx0cy5hZmZlY3RlZFJvd3MpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2soLTEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNyZWF0ZVRhc2sodGFzazpUYXNrLCBjYWxsYmFjazogYW55KXtcclxuICAgICAgICB0aGlzLnBvb2wuZ2V0Q29ubmVjdGlvbihhc3luYyBmdW5jdGlvbihlcnI6YW55LCBjb25uZWN0aW9uOmFueSl7XHJcbiAgICAgICAgICAgIGNvbm5lY3Rpb24ucmVsZWFzZSgpO1xyXG5cclxuICAgICAgICAgICAgaWYoZXJyKSB0aHJvdyBlcnI7XHJcblxyXG4gICAgICAgICAgICBjb25uZWN0aW9uLnF1ZXJ5ID0gdXRpbC5wcm9taXNpZnkoY29ubmVjdGlvbi5xdWVyeSk7XHJcbiAgICAgICAgICAgIGxldCByZXN1bHRzID0gYXdhaXQgY29ubmVjdGlvbi5xdWVyeShcIklOU0VSVCBJTlRPIFRBU0tTIChMSVNUSUQsIE5BTUUsIERFU0NSSVBUSU9OLCBERVBFTkRFTkNJRVMsIFBSSU9SSVRZLCBTVEFUVVMsIERVRURBVEUsIENPTVBMRVRFRFwiLCBbdGFzay5MaXN0SWQsIHRhc2suTmFtZSwgdGFzay5EZXNjcmlwdGlvbiwgdGFzay5EZXBlbmRlbmNpZXMsIHRhc2suUHJpb3JpdHksIHRhc2suU3RhdHVzLCB0YXNrLkR1ZURhdGUsIHRhc2suQ29tcGxldGVkXSk7XHJcbiAgICAgICAgICAgIGlmKHJlc3VsdHMuYWZmZWN0ZWRSb3dzICE9IDEpe1xyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2soLTEpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2socmVzdWx0cy5pbnNlcnRJZCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgdXBkYXRlVGFzayh0YXNrOlRhc2ssIGNhbGxiYWNrOiBhbnkpe1xyXG4gICAgICAgIHRoaXMucG9vbC5nZXRDb25uZWN0aW9uKGFzeW5jIGZ1bmN0aW9uKGVycjphbnksIGNvbm5lY3Rpb246YW55KXtcclxuICAgICAgICAgICAgY29ubmVjdGlvbi5yZWxlYXNlKCk7XHJcblxyXG4gICAgICAgICAgICBpZihlcnIpIHRocm93IGVycjtcclxuXHJcbiAgICAgICAgICAgIGNvbm5lY3Rpb24ucXVlcnkgPSB1dGlsLnByb21pc2lmeShjb25uZWN0aW9uLnF1ZXJ5KTtcclxuICAgICAgICAgICAgbGV0IHJlc3VsdHMgPSBhd2FpdCBjb25uZWN0aW9uLnF1ZXJ5KFwiVVBEQVRFIFRBU0tTIFNFVCBOQU1FID0gPywgREVTQ1JJUFRJT04gPSA/LCBERVBFTkRFTkNJRVMgPSA/LCBQUklPUklUWSA9ID8sIFNUQVRVUyA9ID8sIERVRURBVEUgPSA/LCBDT01QTEVURUQgPSA/IFdIRVJFIFRBU0tJRCA9ID9cIiwgW3Rhc2suTmFtZSwgdGFzay5EZXNjcmlwdGlvbiwgdGFzay5EZXBlbmRlbmNpZXMsIHRhc2suUHJpb3JpdHksIHRhc2suU3RhdHVzLCB0YXNrLkR1ZURhdGUsIHRhc2suQ29tcGxldGVkLCB0YXNrLlRhc2tJZF0pO1xyXG4gICAgICAgICAgICBpZihyZXN1bHRzLmFmZmVjdGVkUm93cyAhPSAxKXtcclxuICAgICAgICAgICAgICAgIGNhbGxiYWNrKC0xKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNhbGxiYWNrKDEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGRlbGV0ZVRhc2sodGFza2lkOm51bWJlciwgY2FsbGJhY2s6IGFueSl7XHJcbiAgICAgICAgdGhpcy5wb29sLmdldENvbm5lY3Rpb24oYXN5bmMgZnVuY3Rpb24oZXJyOmFueSwgY29ubmVjdGlvbjphbnkpe1xyXG4gICAgICAgICAgICBjb25uZWN0aW9uLnJlbGVhc2UoKTtcclxuXHJcbiAgICAgICAgICAgIGlmKGVycikgdGhyb3cgZXJyO1xyXG5cclxuICAgICAgICAgICAgY29ubmVjdGlvbi5xdWVyeSA9IHV0aWwucHJvbWlzaWZ5KGNvbm5lY3Rpb24ucXVlcnkpO1xyXG4gICAgICAgICAgICBsZXQgcmVzdWx0cyA9IGF3YWl0IGNvbm5lY3Rpb24ucXVlcnkoXCJERUxFVEUgRlJPTSBUQVNLUyBXSEVSRSBUQVNLSUQgPSA/XCIsIFt0YXNraWRdKTtcclxuICAgICAgICAgICAgaWYocmVzdWx0cy5hZmZlY3RlZFJvd3MgIT0gMSl7XHJcbiAgICAgICAgICAgICAgICBjYWxsYmFjaygtMSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjYWxsYmFjaygxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBTUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7OzsrQ0FSQSxvSjs7SUFVYUEsTztFQVNULGlCQUFZQyxJQUFaLEVBQXlCQyxJQUF6QixFQUFzQ0MsUUFBdEMsRUFBd0RDLFFBQXhELEVBQXlFO0lBQUE7SUFBQSw0Q0FQbkQsRUFPbUQ7SUFBQSw0Q0FObkQsSUFNbUQ7SUFBQSxnREFML0MsRUFLK0M7SUFBQSxnREFKL0MsRUFJK0M7SUFBQSw4Q0FIakQsWUFHaUQ7SUFBQSw0Q0FGMUQsS0FBS0MsZ0JBQUwsRUFFMEQ7SUFDckUsS0FBS0osSUFBTCxHQUFZQSxJQUFaO0lBQ0EsS0FBS0MsSUFBTCxHQUFZQSxJQUFaO0lBQ0EsS0FBS0MsUUFBTCxHQUFnQkEsUUFBaEI7SUFDQSxLQUFLQyxRQUFMLEdBQWdCQSxRQUFoQjtJQUNBLEtBQUtFLElBQUwsR0FBWSxLQUFLRCxnQkFBTCxFQUFaO0VBQ0g7Ozs7V0FFRCw0QkFDQTtNQUNJLE9BQU9FLEtBQUssQ0FBQ0MsVUFBTixDQUFpQjtRQUFDUCxJQUFJLEVBQUUsS0FBS0EsSUFBWjtRQUFrQkMsSUFBSSxFQUFFLEtBQUtBLElBQTdCO1FBQW1DTyxJQUFJLEVBQUUsS0FBS04sUUFBOUM7UUFBd0RDLFFBQVEsRUFBRSxLQUFLQSxRQUF2RTtRQUFpRk0sUUFBUSxFQUFFLEtBQUtDLE1BQWhHO1FBQXdHQyxlQUFlLEVBQUU7TUFBekgsQ0FBakIsQ0FBUDtJQUNIOzs7V0FFRCx3QkFBc0JDLE1BQXRCLEVBQXFDQyxRQUFyQyxFQUFrRDtNQUM5QyxJQUFJQyxLQUFZLEdBQUcsRUFBbkI7TUFFQSxLQUFLVCxJQUFMLENBQVVVLGFBQVY7UUFBQSxvRkFBd0IsaUJBQWVDLEdBQWYsRUFBd0JDLFVBQXhCO1VBQUE7VUFBQTtZQUFBO2NBQUE7Z0JBQUE7a0JBQ3BCQSxVQUFVLENBQUNDLE9BQVg7O2tCQURvQixLQUdqQkYsR0FIaUI7b0JBQUE7b0JBQUE7a0JBQUE7O2tCQUFBLE1BR05BLEdBSE07O2dCQUFBO2tCQUtwQkMsVUFBVSxDQUFDRSxLQUFYLEdBQW1CQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUosVUFBVSxDQUFDRSxLQUExQixDQUFuQjtrQkFDQUcsT0FBTyxDQUFDQyxHQUFSLENBQVlYLE1BQVo7a0JBTm9CO2tCQUFBLE9BT0FLLFVBQVUsQ0FBQ0UsS0FBWCxDQUFpQixzQ0FBakIsRUFBeUQsQ0FBQ1AsTUFBRCxDQUF6RCxDQVBBOztnQkFBQTtrQkFPaEJZLE9BUGdCO2tCQVFwQkYsT0FBTyxDQUFDQyxHQUFSLENBQVlDLE9BQVo7O2tCQUNBLEtBQVFDLENBQVIsR0FBWSxDQUFaLEVBQWVBLENBQUMsR0FBR0QsT0FBTyxDQUFDRSxNQUEzQixFQUFtQ0QsQ0FBQyxFQUFwQyxFQUF1QztvQkFDbkNYLEtBQUssQ0FBQ2EsSUFBTixDQUFXLElBQUlDLFVBQUosQ0FBU2hCLE1BQVQsRUFBaUJZLE9BQU8sQ0FBQ0MsQ0FBRCxDQUFQLENBQVdJLFFBQTVCLEVBQXNDTCxPQUFPLENBQUNDLENBQUQsQ0FBUCxDQUFXSyxNQUFqRCxDQUFYO2tCQUNIOztrQkFFRGpCLFFBQVEsQ0FBQ0MsS0FBRCxDQUFSOztnQkFib0I7Z0JBQUE7a0JBQUE7Y0FBQTtZQUFBO1VBQUE7UUFBQSxDQUF4Qjs7UUFBQTtVQUFBO1FBQUE7TUFBQTtJQWVIOzs7V0FFRCxzQkFBb0JOLElBQXBCLEVBQStCSyxRQUEvQixFQUE0QztNQUN4QyxJQUFJRCxNQUFNLEdBQUcsQ0FBQyxDQUFkO01BRUEsS0FBS1AsSUFBTCxDQUFVVSxhQUFWO1FBQUEscUZBQXdCLGtCQUFlQyxHQUFmLEVBQXdCQyxVQUF4QjtVQUFBO1VBQUE7WUFBQTtjQUFBO2dCQUFBO2tCQUNwQkEsVUFBVSxDQUFDQyxPQUFYOztrQkFEb0IsS0FHakJGLEdBSGlCO29CQUFBO29CQUFBO2tCQUFBOztrQkFBQSxNQUdOQSxHQUhNOztnQkFBQTtrQkFLcEJDLFVBQVUsQ0FBQ0UsS0FBWCxHQUFtQkMsSUFBSSxDQUFDQyxTQUFMLENBQWVKLFVBQVUsQ0FBQ0UsS0FBMUIsQ0FBbkI7a0JBTG9CO2tCQUFBLE9BTUFGLFVBQVUsQ0FBQ0UsS0FBWCxDQUFpQiw4REFBakIsRUFBaUYsQ0FBQ1gsSUFBSSxDQUFDdUIsUUFBTixFQUFnQnZCLElBQUksQ0FBQ3dCLFFBQXJCLENBQWpGLENBTkE7O2dCQUFBO2tCQU1oQlIsT0FOZ0I7O2tCQU9wQixJQUFHQSxPQUFPLENBQUNFLE1BQVIsSUFBa0IsQ0FBckIsRUFBdUI7b0JBQ25CZCxNQUFNLEdBQUdZLE9BQU8sQ0FBQyxDQUFELENBQVAsQ0FBV1MsTUFBcEI7a0JBQ0g7O2tCQUNEcEIsUUFBUSxDQUFDRCxNQUFELENBQVI7O2dCQVZvQjtnQkFBQTtrQkFBQTtjQUFBO1lBQUE7VUFBQTtRQUFBLENBQXhCOztRQUFBO1VBQUE7UUFBQTtNQUFBO0lBY0g7OztXQUVELHVCQUFxQmtCLE1BQXJCLEVBQW9DakIsUUFBcEMsRUFBaUQ7TUFDN0MsSUFBSXFCLEtBQVksR0FBRyxFQUFuQjtNQUVBLEtBQUs3QixJQUFMLENBQVVVLGFBQVY7UUFBQSxxRkFBd0Isa0JBQWVDLEdBQWYsRUFBd0JDLFVBQXhCO1VBQUE7VUFBQTtZQUFBO2NBQUE7Z0JBQUE7a0JBQ3BCQSxVQUFVLENBQUNDLE9BQVg7O2tCQURvQixLQUdqQkYsR0FIaUI7b0JBQUE7b0JBQUE7a0JBQUE7O2tCQUFBLE1BR05BLEdBSE07O2dCQUFBO2tCQUtwQkMsVUFBVSxDQUFDRSxLQUFYLEdBQW1CQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUosVUFBVSxDQUFDRSxLQUExQixDQUFuQjtrQkFMb0I7a0JBQUEsT0FNQUYsVUFBVSxDQUFDRSxLQUFYLENBQWlCLHNDQUFqQixFQUF5RCxDQUFDVyxNQUFELENBQXpELENBTkE7O2dCQUFBO2tCQU1oQk4sT0FOZ0I7O2tCQU9wQixLQUFRQyxDQUFSLEdBQVksQ0FBWixFQUFlQSxDQUFDLEdBQUdELE9BQU8sQ0FBQ0UsTUFBM0IsRUFBbUNELENBQUMsRUFBcEMsRUFBdUM7b0JBQ25DUyxLQUFLLENBQUNQLElBQU4sQ0FBVyxJQUFJUSxVQUFKLENBQVNMLE1BQVQsRUFBaUJOLE9BQU8sQ0FBQ0MsQ0FBRCxDQUFQLENBQVdXLElBQTVCLEVBQWtDWixPQUFPLENBQUNDLENBQUQsQ0FBUCxDQUFXWSxXQUE3QyxFQUEwRGIsT0FBTyxDQUFDQyxDQUFELENBQVAsQ0FBV2EsWUFBckUsRUFBbUZkLE9BQU8sQ0FBQ0MsQ0FBRCxDQUFQLENBQVdjLFFBQTlGLEVBQXdHZixPQUFPLENBQUNDLENBQUQsQ0FBUCxDQUFXZSxNQUFuSCxFQUEySGhCLE9BQU8sQ0FBQ0MsQ0FBRCxDQUFQLENBQVdnQixPQUF0SSxFQUErSWpCLE9BQU8sQ0FBQ0MsQ0FBRCxDQUFQLENBQVdpQixTQUExSixFQUFxS2xCLE9BQU8sQ0FBQ0MsQ0FBRCxDQUFQLENBQVdrQixNQUFoTCxDQUFYO2tCQUNIOztrQkFFRDlCLFFBQVEsQ0FBQ3FCLEtBQUQsQ0FBUjs7Z0JBWG9CO2dCQUFBO2tCQUFBO2NBQUE7WUFBQTtVQUFBO1FBQUEsQ0FBeEI7O1FBQUE7VUFBQTtRQUFBO01BQUE7SUFhSDs7O1dBRUQsb0JBQWtCMUIsSUFBbEIsRUFBNkJLLFFBQTdCLEVBQTBDO01BQ3RDLEtBQUtSLElBQUwsQ0FBVVUsYUFBVjtRQUFBLHFGQUF3QixrQkFBZUMsR0FBZixFQUF3QkMsVUFBeEI7VUFBQTtVQUFBO1lBQUE7Y0FBQTtnQkFBQTtrQkFDcEJBLFVBQVUsQ0FBQ0MsT0FBWDs7a0JBRG9CLEtBR2pCRixHQUhpQjtvQkFBQTtvQkFBQTtrQkFBQTs7a0JBQUEsTUFHTkEsR0FITTs7Z0JBQUE7a0JBS3BCQyxVQUFVLENBQUNFLEtBQVgsR0FBbUJDLElBQUksQ0FBQ0MsU0FBTCxDQUFlSixVQUFVLENBQUNFLEtBQTFCLENBQW5CO2tCQUxvQjtrQkFBQSxPQU1BRixVQUFVLENBQUNFLEtBQVgsQ0FBaUIsb0VBQWpCLEVBQXVGLENBQUNYLElBQUksQ0FBQ3VCLFFBQU4sRUFBZ0J2QixJQUFJLENBQUNvQyxZQUFyQixFQUFtQ3BDLElBQUksQ0FBQ3dCLFFBQXhDLENBQXZGLENBTkE7O2dCQUFBO2tCQU1oQlIsT0FOZ0I7O2tCQU9wQixJQUFHQSxPQUFPLENBQUNxQixZQUFSLElBQXdCLENBQTNCLEVBQTZCO29CQUN6QmhDLFFBQVEsQ0FBQyxDQUFDLENBQUYsQ0FBUjtrQkFDSCxDQUZELE1BRU87b0JBQ0hBLFFBQVEsQ0FBQ1csT0FBTyxDQUFDc0IsUUFBVCxDQUFSO2tCQUNIOztnQkFYbUI7Z0JBQUE7a0JBQUE7Y0FBQTtZQUFBO1VBQUE7UUFBQSxDQUF4Qjs7UUFBQTtVQUFBO1FBQUE7TUFBQTtJQWFIOzs7V0FFRCxvQkFBa0J0QyxJQUFsQixFQUE2QkssUUFBN0IsRUFBMEM7TUFDdEMsS0FBS1IsSUFBTCxDQUFVVSxhQUFWO1FBQUEscUZBQXdCLGtCQUFlQyxHQUFmLEVBQXdCQyxVQUF4QjtVQUFBO1VBQUE7WUFBQTtjQUFBO2dCQUFBO2tCQUNwQkEsVUFBVSxDQUFDQyxPQUFYOztrQkFEb0IsS0FHakJGLEdBSGlCO29CQUFBO29CQUFBO2tCQUFBOztrQkFBQSxNQUdOQSxHQUhNOztnQkFBQTtrQkFLcEJDLFVBQVUsQ0FBQ0UsS0FBWCxHQUFtQkMsSUFBSSxDQUFDQyxTQUFMLENBQWVKLFVBQVUsQ0FBQ0UsS0FBMUIsQ0FBbkI7a0JBTG9CO2tCQUFBLE9BTUFGLFVBQVUsQ0FBQ0UsS0FBWCxDQUFpQixnRkFBakIsRUFBbUcsQ0FBQ1gsSUFBSSxDQUFDdUIsUUFBTixFQUFnQnZCLElBQUksQ0FBQ29DLFlBQXJCLEVBQW1DcEMsSUFBSSxDQUFDd0IsUUFBeEMsRUFBa0R4QixJQUFJLENBQUN1QyxNQUF2RCxDQUFuRyxDQU5BOztnQkFBQTtrQkFNaEJ2QixPQU5nQjs7a0JBT3BCLElBQUdBLE9BQU8sQ0FBQ3FCLFlBQVIsSUFBd0IsQ0FBM0IsRUFBNkI7b0JBQ3pCaEMsUUFBUSxDQUFDLENBQUMsQ0FBRixDQUFSO2tCQUNILENBRkQsTUFFTTtvQkFDRkEsUUFBUSxDQUFDLENBQUQsQ0FBUjtrQkFDSDs7Z0JBWG1CO2dCQUFBO2tCQUFBO2NBQUE7WUFBQTtVQUFBO1FBQUEsQ0FBeEI7O1FBQUE7VUFBQTtRQUFBO01BQUE7SUFhSDs7O1dBRUQsb0JBQWtCRCxNQUFsQixFQUFpQ0MsUUFBakMsRUFBK0M7TUFDM0MsS0FBS1IsSUFBTCxDQUFVVSxhQUFWO1FBQUEscUZBQXdCLGtCQUFlQyxHQUFmLEVBQXdCQyxVQUF4QjtVQUFBO1VBQUE7WUFBQTtjQUFBO2dCQUFBO2tCQUNwQkEsVUFBVSxDQUFDQyxPQUFYOztrQkFEb0IsS0FHakJGLEdBSGlCO29CQUFBO29CQUFBO2tCQUFBOztrQkFBQSxNQUdOQSxHQUhNOztnQkFBQTtrQkFLcEJDLFVBQVUsQ0FBQ0UsS0FBWCxHQUFtQkMsSUFBSSxDQUFDQyxTQUFMLENBQWVKLFVBQVUsQ0FBQ0UsS0FBMUIsQ0FBbkI7a0JBTG9CO2tCQUFBLE9BTUFGLFVBQVUsQ0FBQ0UsS0FBWCxDQUFpQixvQ0FBakIsRUFBdUQsQ0FBQ1AsTUFBRCxDQUF2RCxDQU5BOztnQkFBQTtrQkFNaEJZLE9BTmdCOztrQkFPcEIsSUFBR0EsT0FBTyxDQUFDcUIsWUFBUixJQUF3QixDQUEzQixFQUE2QjtvQkFDekJoQyxRQUFRLENBQUMsQ0FBQyxDQUFGLENBQVI7a0JBQ0gsQ0FGRCxNQUVPO29CQUNIQSxRQUFRLENBQUMsQ0FBRCxDQUFSO2tCQUNIOztnQkFYbUI7Z0JBQUE7a0JBQUE7Y0FBQTtZQUFBO1VBQUE7UUFBQSxDQUF4Qjs7UUFBQTtVQUFBO1FBQUE7TUFBQTtJQWFIOzs7V0FFRCxvQkFBa0JtQyxJQUFsQixFQUE2Qm5DLFFBQTdCLEVBQTBDO01BQ3RDLEtBQUtSLElBQUwsQ0FBVVUsYUFBVjtRQUFBLHFGQUF3QixrQkFBZUMsR0FBZixFQUF3QkMsVUFBeEI7VUFBQTtVQUFBO1lBQUE7Y0FBQTtnQkFBQTtrQkFDcEJBLFVBQVUsQ0FBQ0MsT0FBWDs7a0JBRG9CLEtBR2pCRixHQUhpQjtvQkFBQTtvQkFBQTtrQkFBQTs7a0JBQUEsTUFHTkEsR0FITTs7Z0JBQUE7a0JBS3BCQyxVQUFVLENBQUNFLEtBQVgsR0FBbUJDLElBQUksQ0FBQ0MsU0FBTCxDQUFlSixVQUFVLENBQUNFLEtBQTFCLENBQW5CO2tCQUxvQjtrQkFBQSxPQU1BRixVQUFVLENBQUNFLEtBQVgsQ0FBaUIsa0RBQWpCLEVBQXFFLENBQUM2QixJQUFJLENBQUNELE1BQU4sRUFBY0MsSUFBSSxDQUFDQyxRQUFuQixDQUFyRSxDQU5BOztnQkFBQTtrQkFNaEJ6QixPQU5nQjs7a0JBT3BCLElBQUdBLE9BQU8sQ0FBQ3FCLFlBQVIsSUFBd0IsQ0FBM0IsRUFBNkI7b0JBQ3pCaEMsUUFBUSxDQUFDLENBQUMsQ0FBRixDQUFSO2tCQUNILENBRkQsTUFFTztvQkFDSEEsUUFBUSxDQUFDVyxPQUFPLENBQUNzQixRQUFULENBQVI7a0JBQ0g7O2dCQVhtQjtnQkFBQTtrQkFBQTtjQUFBO1lBQUE7VUFBQTtRQUFBLENBQXhCOztRQUFBO1VBQUE7UUFBQTtNQUFBO0lBYUg7OztXQUVELG9CQUFrQkUsSUFBbEIsRUFBNkJuQyxRQUE3QixFQUEwQztNQUN0QyxLQUFLUixJQUFMLENBQVVVLGFBQVY7UUFBQSxxRkFBd0Isa0JBQWVDLEdBQWYsRUFBd0JDLFVBQXhCO1VBQUE7VUFBQTtZQUFBO2NBQUE7Z0JBQUE7a0JBQ3BCQSxVQUFVLENBQUNDLE9BQVg7O2tCQURvQixLQUdqQkYsR0FIaUI7b0JBQUE7b0JBQUE7a0JBQUE7O2tCQUFBLE1BR05BLEdBSE07O2dCQUFBO2tCQUtwQkMsVUFBVSxDQUFDRSxLQUFYLEdBQW1CQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUosVUFBVSxDQUFDRSxLQUExQixDQUFuQjtrQkFMb0I7a0JBQUEsT0FNQUYsVUFBVSxDQUFDRSxLQUFYLENBQWlCLGdEQUFqQixFQUFtRSxDQUFDNkIsSUFBSSxDQUFDRSxNQUFOLENBQW5FLENBTkE7O2dCQUFBO2tCQU1oQjFCLE9BTmdCOztrQkFPcEIsSUFBR0EsT0FBTyxDQUFDcUIsWUFBUixJQUF3QixDQUEzQixFQUE2QjtvQkFDekJoQyxRQUFRLENBQUMsQ0FBQyxDQUFGLENBQVI7a0JBQ0gsQ0FGRCxNQUVPO29CQUNIQSxRQUFRLENBQUMsQ0FBRCxDQUFSO2tCQUNIOztnQkFYbUI7Z0JBQUE7a0JBQUE7Y0FBQTtZQUFBO1VBQUE7UUFBQSxDQUF4Qjs7UUFBQTtVQUFBO1FBQUE7TUFBQTtJQWFIOzs7V0FFRCxvQkFBa0JpQixNQUFsQixFQUFpQ2pCLFFBQWpDLEVBQThDO01BQzFDLEtBQUtSLElBQUwsQ0FBVVUsYUFBVjtRQUFBLHFGQUF3QixrQkFBZUMsR0FBZixFQUF3QkMsVUFBeEI7VUFBQTtVQUFBO1lBQUE7Y0FBQTtnQkFBQTtrQkFDcEJBLFVBQVUsQ0FBQ0MsT0FBWDtrQkFDQUksT0FBTyxDQUFDQyxHQUFSLENBQVkscUJBQVo7O2tCQUZvQixLQUdqQlAsR0FIaUI7b0JBQUE7b0JBQUE7a0JBQUE7O2tCQUFBLE1BR05BLEdBSE07O2dCQUFBO2tCQUtwQkMsVUFBVSxDQUFDRSxLQUFYLEdBQW1CQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUosVUFBVSxDQUFDRSxLQUExQixDQUFuQjtrQkFMb0I7a0JBQUEsT0FNSUYsVUFBVSxDQUFDRSxLQUFYLENBQWlCLG9DQUFqQixFQUF1RCxDQUFDVyxNQUFELENBQXZELENBTko7O2dCQUFBO2tCQU1oQnFCLFdBTmdCO2tCQU9wQjdCLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGVBQVo7a0JBQ0FELE9BQU8sQ0FBQ0MsR0FBUixDQUFZNEIsV0FBVyxDQUFDTixZQUF4Qjs7a0JBUm9CLE1BU2pCTSxXQUFXLENBQUNOLFlBQVosR0FBMkIsQ0FBQyxDQVRYO29CQUFBO29CQUFBO2tCQUFBOztrQkFBQTtrQkFBQSxPQVVRNUIsVUFBVSxDQUFDRSxLQUFYLENBQWlCLG9DQUFqQixFQUF1RCxDQUFDVyxNQUFELENBQXZELENBVlI7O2dCQUFBO2tCQVVac0IsV0FWWTs7a0JBV2hCLElBQUdBLFdBQVcsQ0FBQ1AsWUFBWixJQUE0QixDQUEvQixFQUFpQztvQkFDN0JoQyxRQUFRLENBQUMsQ0FBQyxDQUFGLENBQVI7a0JBQ0gsQ0FGRCxNQUVPO29CQUNIQSxRQUFRLENBQUNzQyxXQUFXLENBQUNOLFlBQVosR0FBMkJPLFdBQVcsQ0FBQ1AsWUFBeEMsQ0FBUjtrQkFDSDs7a0JBZmU7a0JBQUE7O2dCQUFBO2tCQWlCaEJoQyxRQUFRLENBQUMsQ0FBQyxDQUFGLENBQVI7O2dCQWpCZ0I7Z0JBQUE7a0JBQUE7Y0FBQTtZQUFBO1VBQUE7UUFBQSxDQUF4Qjs7UUFBQTtVQUFBO1FBQUE7TUFBQTtJQW9CSDs7O1dBRUQsb0JBQWtCd0MsSUFBbEIsRUFBNkJ4QyxRQUE3QixFQUEyQztNQUN2QyxLQUFLUixJQUFMLENBQVVVLGFBQVY7UUFBQSxzRkFBd0IsbUJBQWVDLEdBQWYsRUFBd0JDLFVBQXhCO1VBQUE7VUFBQTtZQUFBO2NBQUE7Z0JBQUE7a0JBQ3BCQSxVQUFVLENBQUNDLE9BQVg7O2tCQURvQixLQUdqQkYsR0FIaUI7b0JBQUE7b0JBQUE7a0JBQUE7O2tCQUFBLE1BR05BLEdBSE07O2dCQUFBO2tCQUtwQkMsVUFBVSxDQUFDRSxLQUFYLEdBQW1CQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUosVUFBVSxDQUFDRSxLQUExQixDQUFuQjtrQkFMb0I7a0JBQUEsT0FNQUYsVUFBVSxDQUFDRSxLQUFYLENBQWlCLGtHQUFqQixFQUFxSCxDQUFDa0MsSUFBSSxDQUFDSCxNQUFOLEVBQWNHLElBQUksQ0FBQ0MsSUFBbkIsRUFBeUJELElBQUksQ0FBQ0UsV0FBOUIsRUFBMkNGLElBQUksQ0FBQ0csWUFBaEQsRUFBOERILElBQUksQ0FBQ0ksUUFBbkUsRUFBNkVKLElBQUksQ0FBQ0ssTUFBbEYsRUFBMEZMLElBQUksQ0FBQ00sT0FBL0YsRUFBd0dOLElBQUksQ0FBQ08sU0FBN0csQ0FBckgsQ0FOQTs7Z0JBQUE7a0JBTWhCcEMsT0FOZ0I7O2tCQU9wQixJQUFHQSxPQUFPLENBQUNxQixZQUFSLElBQXdCLENBQTNCLEVBQTZCO29CQUN6QmhDLFFBQVEsQ0FBQyxDQUFDLENBQUYsQ0FBUjtrQkFDSCxDQUZELE1BRU87b0JBQ0hBLFFBQVEsQ0FBQ1csT0FBTyxDQUFDc0IsUUFBVCxDQUFSO2tCQUNIOztnQkFYbUI7Z0JBQUE7a0JBQUE7Y0FBQTtZQUFBO1VBQUE7UUFBQSxDQUF4Qjs7UUFBQTtVQUFBO1FBQUE7TUFBQTtJQWFIOzs7V0FFRCxvQkFBa0JPLElBQWxCLEVBQTZCeEMsUUFBN0IsRUFBMkM7TUFDdkMsS0FBS1IsSUFBTCxDQUFVVSxhQUFWO1FBQUEsc0ZBQXdCLG1CQUFlQyxHQUFmLEVBQXdCQyxVQUF4QjtVQUFBO1VBQUE7WUFBQTtjQUFBO2dCQUFBO2tCQUNwQkEsVUFBVSxDQUFDQyxPQUFYOztrQkFEb0IsS0FHakJGLEdBSGlCO29CQUFBO29CQUFBO2tCQUFBOztrQkFBQSxNQUdOQSxHQUhNOztnQkFBQTtrQkFLcEJDLFVBQVUsQ0FBQ0UsS0FBWCxHQUFtQkMsSUFBSSxDQUFDQyxTQUFMLENBQWVKLFVBQVUsQ0FBQ0UsS0FBMUIsQ0FBbkI7a0JBTG9CO2tCQUFBLE9BTUFGLFVBQVUsQ0FBQ0UsS0FBWCxDQUFpQixxSUFBakIsRUFBd0osQ0FBQ2tDLElBQUksQ0FBQ0MsSUFBTixFQUFZRCxJQUFJLENBQUNFLFdBQWpCLEVBQThCRixJQUFJLENBQUNHLFlBQW5DLEVBQWlESCxJQUFJLENBQUNJLFFBQXRELEVBQWdFSixJQUFJLENBQUNLLE1BQXJFLEVBQTZFTCxJQUFJLENBQUNNLE9BQWxGLEVBQTJGTixJQUFJLENBQUNPLFNBQWhHLEVBQTJHUCxJQUFJLENBQUNRLE1BQWhILENBQXhKLENBTkE7O2dCQUFBO2tCQU1oQnJDLE9BTmdCOztrQkFPcEIsSUFBR0EsT0FBTyxDQUFDcUIsWUFBUixJQUF3QixDQUEzQixFQUE2QjtvQkFDekJoQyxRQUFRLENBQUMsQ0FBQyxDQUFGLENBQVI7a0JBQ0gsQ0FGRCxNQUVPO29CQUNIQSxRQUFRLENBQUMsQ0FBRCxDQUFSO2tCQUNIOztnQkFYbUI7Z0JBQUE7a0JBQUE7Y0FBQTtZQUFBO1VBQUE7UUFBQSxDQUF4Qjs7UUFBQTtVQUFBO1FBQUE7TUFBQTtJQWFIOzs7V0FFRCxvQkFBa0I4QixNQUFsQixFQUFpQzlCLFFBQWpDLEVBQStDO01BQzNDLEtBQUtSLElBQUwsQ0FBVVUsYUFBVjtRQUFBLHNGQUF3QixtQkFBZUMsR0FBZixFQUF3QkMsVUFBeEI7VUFBQTtVQUFBO1lBQUE7Y0FBQTtnQkFBQTtrQkFDcEJBLFVBQVUsQ0FBQ0MsT0FBWDs7a0JBRG9CLEtBR2pCRixHQUhpQjtvQkFBQTtvQkFBQTtrQkFBQTs7a0JBQUEsTUFHTkEsR0FITTs7Z0JBQUE7a0JBS3BCQyxVQUFVLENBQUNFLEtBQVgsR0FBbUJDLElBQUksQ0FBQ0MsU0FBTCxDQUFlSixVQUFVLENBQUNFLEtBQTFCLENBQW5CO2tCQUxvQjtrQkFBQSxPQU1BRixVQUFVLENBQUNFLEtBQVgsQ0FBaUIsb0NBQWpCLEVBQXVELENBQUN3QixNQUFELENBQXZELENBTkE7O2dCQUFBO2tCQU1oQm5CLE9BTmdCOztrQkFPcEIsSUFBR0EsT0FBTyxDQUFDcUIsWUFBUixJQUF3QixDQUEzQixFQUE2QjtvQkFDekJoQyxRQUFRLENBQUMsQ0FBQyxDQUFGLENBQVI7a0JBQ0gsQ0FGRCxNQUVPO29CQUNIQSxRQUFRLENBQUMsQ0FBRCxDQUFSO2tCQUNIOztnQkFYbUI7Z0JBQUE7a0JBQUE7Y0FBQTtZQUFBO1VBQUE7UUFBQSxDQUF4Qjs7UUFBQTtVQUFBO1FBQUE7TUFBQTtJQWFIIn0=