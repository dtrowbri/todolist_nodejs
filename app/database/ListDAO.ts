/*
    Author: Donald Trowbridge
    Notes: still don't have a working environment. I have no idea if the code runs.
*/

import {User} from "../models/User";
import {List} from "../models/List";
import { Task } from "../models/Task";
import * as mysql from "mysql";
import * as util from "util";

export class ListDAO{

    private host:string = "";
    private port:number = 3306;
    private username:string = "";
    private password:string = "";
    private schema:string = "mytodolist";
    private pool = this.initDbConnection();

    constructor(host:string, port:number, username: string, password: string){
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.pool = this.initDbConnection();
    }

    private initDbConnection():any
    {
        return mysql.createPool({host: this.host, port: this.port, user: this.username, password: this.password, database: this.schema, connectionLimit: 10});
    }

    public findUsersLists(userid:number, callback:any){
        let lists:List[] = [];

        this.pool.getConnection(async function(err:any, connection:any){
            connection.release();

            if(err) throw err;

            connection.query = util.promisify(connection.query);
            console.log(userid);
            let results = await connection.query('SELECT * FROM LISTS WHERE USERID = ?', [userid]);
            console.log(results);
            for(let x = 0; x < results.length; x++){
                lists.push(new List(userid, results[x].listname, results[x].listid));
            }

            callback(lists);
        });
    }

    public authenticate(user:User, callback:any){
        let userid = -1;

        this.pool.getConnection(async function(err:any, connection:any){
            connection.release();

            if(err) throw err;

            connection.query = util.promisify(connection.query);
            let results = await connection.query("SELECT USERID FROM USERS WHERE USERNAME = ? AND PASSWORD = ?", [user.UserName, user.Password]);
            if(results.length == 1){
                userid = results[0].USERID;
            }
            callback(userid);
        })
        
        
    }

    public findListTasks(listid:number, callback:any){
        let tasks:Task[] = [];
        
        this.pool.getConnection(async function(err:any, connection:any){
            connection.release();

            if(err) throw err;

            connection.query = util.promisify(connection.query);
            let results = await connection.query("SELECT * FROM TASKS WHERE LISTID = ?", [listid]);
            for(let x = 0; x < results.length; x++){
                tasks.push(new Task(listid, results[x].name, results[x].description, results[x].dependencies, results[x].priority, results[x].status, results[x].duedate, results[x].completed, results[x].taskid));
            }

            callback(tasks);
        });
    }

    public createUser(user:User, callback:any){
        this.pool.getConnection(async function(err:any, connection:any){
            connection.release();

            if(err) throw err;

            connection.query = util.promisify(connection.query);
            let results = await connection.query('INSERT INTO USERS (USERNAME, EMAILADDRESS, PASSWORD) VALUES(?,?,?)', [user.UserName, user.EmailAddress, user.Password]);
            if(results.affectedRows != 1){
                callback(-1);
            } else {
                callback(results.insertId);
            }
        });
    }

    public updateUser(user:User, callback:any){
        this.pool.getConnection(async function(err:any, connection: any){
            connection.release();

            if(err) throw err;

            connection.query = util.promisify(connection.query);
            let results = await connection.query("UPDATE USERS SET USERNAME = ?, EMAILADDRESS = ?, PASSWORD = ? WHERE USERID = ?", [user.UserName, user.EmailAddress, user.Password, user.UserId]);
            if(results.affectedRows != 1){
                callback(-1);
            }else {
                callback(1);
            }
        });
    }

    public deleteUser(userid:number, callback: any){
        this.pool.getConnection(async function(err:any, connection: any){
            connection.release();

            if(err) throw err;

            connection.query = util.promisify(connection.query);
            let results = await connection.query("DELETE FROM USERS WHERE USERID = ?", [userid]);
            if(results.affectedRows != 1){
                callback(-1);
            } else {
                callback(1);
            }
        });
    }

    public createList(list:List, callback:any){
        this.pool.getConnection(async function(err:any, connection: any){
            connection.release();

            if(err) throw err;

            connection.query = util.promisify(connection.query);
            let results = await connection.query("INSERT INTO LISTS (USERID, LISTNAME) VALUES(?,?)", [list.UserId, list.ListName]);
            if(results.affectedRows != 1){
                callback(-1);
            } else {
                callback(results.insertId);
            }
        });
    }

    public updateList(list:List, callback:any){
        this.pool.getConnection(async function(err:any, connection: any){
            connection.release();

            if(err) throw err;

            connection.query = util.promisify(connection.query);
            let results = await connection.query("UPDATE LISTS SET LISTNAME = ? WHERE LISTID = ?", [list.ListId]);
            if(results.affectedRows != 1){
                callback(-1);
            } else {
                callback(1);
            }
        });
    }

    public deleteList(listid:number, callback:any){
        this.pool.getConnection(async function(err:any, connection: any){
            connection.release();
            console.log("connection released");
            if(err) throw err;

            connection.query = util.promisify(connection.query);
            let taskResults = await connection.query("DELETE FROM TASKS WHERE LISTID = ?", [listid]);
            console.log("task results ");
            console.log(taskResults.affectedRows);
            if(taskResults.affectedRows > -1){
                let listResults = await connection.query("DELETE FROM LISTS WHERE LISTID = ?", [listid]);
                if(listResults.affectedRows != 1){
                    callback(-1);
                } else {
                    callback(taskResults.affectedRows + listResults.affectedRows);
                }
            } else {
                callback(-1);
            }
        });
    }

    public createTask(task:Task, callback: any){
        this.pool.getConnection(async function(err:any, connection:any){
            connection.release();

            if(err) throw err;

            connection.query = util.promisify(connection.query);
            let results = await connection.query("INSERT INTO TASKS (LISTID, NAME, DESCRIPTION, DEPENDENCIES, PRIORITY, STATUS, DUEDATE, COMPLETED", [task.ListId, task.Name, task.Description, task.Dependencies, task.Priority, task.Status, task.DueDate, task.Completed]);
            if(results.affectedRows != 1){
                callback(-1);
            } else {
                callback(results.insertId);
            }
        });
    }

    public updateTask(task:Task, callback: any){
        this.pool.getConnection(async function(err:any, connection:any){
            connection.release();

            if(err) throw err;

            connection.query = util.promisify(connection.query);
            let results = await connection.query("UPDATE TASKS SET NAME = ?, DESCRIPTION = ?, DEPENDENCIES = ?, PRIORITY = ?, STATUS = ?, DUEDATE = ?, COMPLETED = ? WHERE TASKID = ?", [task.Name, task.Description, task.Dependencies, task.Priority, task.Status, task.DueDate, task.Completed, task.TaskId]);
            if(results.affectedRows != 1){
                callback(-1);
            } else {
                callback(1);
            }
        });
    }

    public deleteTask(taskid:number, callback: any){
        this.pool.getConnection(async function(err:any, connection:any){
            connection.release();

            if(err) throw err;

            connection.query = util.promisify(connection.query);
            let results = await connection.query("DELETE FROM TASKS WHERE TASKID = ?", [taskid]);
            if(results.affectedRows != 1){
                callback(-1);
            } else {
                callback(1);
            }
        });
    }
}