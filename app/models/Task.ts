export class Task {

    private taskid: number;
    private listid: number;
    private name: string;
    private description: string;
    private dependencies: string;
    private priority: number;
    private status: number;
    private duedate: Date;
    private completed: boolean;

    constructor(listid: number, name: string, description: string, dependencies: string, priority: number, status: number, duedate: Date, completed: boolean, taskid? : number){
        this.listid = listid;
        this.name = name;
        this.description = description;
        this.dependencies = dependencies;
        this.priority = priority;
        this.status = status;
        this.duedate = duedate;
        this.completed = completed;

        if(taskid){
            this.taskid = taskid;
        } else {
            this.taskid = -1;
        }
    }

    get TaskId():number {
        return this.taskid;
    }

    set TaskId(taskid: number){
        this.taskid = taskid;
    }

    get ListId():number {
        return this.listid;
    }

    set ListId(listid: number){
        this.listid = listid;
    }

    get Name():string {
        return this.name;
    }

    set Name(name: string){
        this.name = name;
    }

    get Description(): string {
        return this.description;
    }

    set Description(description: string){
        this.description = description;
    }

    get Dependencies(): string{
        return this.dependencies;
    }

    set Dependencies(dependencies: string){
        this.dependencies = dependencies;
    }

    get Priority():number {
        return this.priority;
    }

    set Priority(priority: number){
        this.priority = priority;
    }

    get Status():number {
        return this.status;
    }

    set Status(status: number){
        this.status = status;
    }

    get DueDate():Date {
        return this.DueDate;
    }

    set DueDate(duedate:Date){
        this.duedate = duedate;
    }

    get Completed(): boolean {
        return this.completed;
    }

    set Completed(completed: boolean){
        this.completed = completed;
    }
}