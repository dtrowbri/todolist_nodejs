export class List {

    private listid: number;
    private userid: number;
    private listname: string;

    constructor(userid: number, listname: string, listid? : number){
        this.userid = userid;
        this.listname = listname;

        if(listid){
            this.listid = listid;
        } else {
            this.listid = -1;
        }
    }

    get ListId(): number {
        return this.ListId;
    }

    set ListId(listid: number){
        this.listid = listid;
    }

    get UserId(): number {
        return this.userid;
    }

    set UserId(userid: number){
        this.userid = userid;
    }

    get ListName(): string {
        return this.listname;
    }

    set ListName(listname: string){
        this.listname = listname;
    }
}