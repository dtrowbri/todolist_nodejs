export class User {

    private userid: number;
    private username: string;
    private emailaddress: string;
    private password: string;

    constructor(username:string, emailaddress: string, password: string, userid? : number){
        this.username = username;
        this.emailaddress = emailaddress;
        this.password = password;

        if(userid){
            this.userid = userid;
        } else {
            this.userid = -1;
        }
    }

    get UserId(): number {
        return this.UserId;
    }

    set UserId(userid: number){
        this.userid = userid;
    }

    get UserName():string {
        return this.username;
    }

    set UserName(username: string){
        this.username = username;
    }

    get EmailAddress(): string {
        return this.emailaddress;
    }

    set EmailAddress(emailaddress: string){
        this.emailaddress = emailaddress;
    }

    get Password(): string {
        return this.password;
    }

    set Password(password: string){
        this.password = password;
    }

}